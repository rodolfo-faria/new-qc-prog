#include "int1e.hpp"

Matrix Int1e::compute_obint(const BasisSet& obs, const AtomVector& atoms, OperatorType op_type)
{
	auto shell2bf = obs.shell2bf(); // maps shell index to basis function index
                                // shell2bf[0] = index of the first basis function in shell 0
                                // shell2bf[1] = index of the first basis function in shell 1
                                // ...
                                //
	// get the number of basis functions
	const auto nbf = obs.nbf();
	Matrix result(nbf, nbf);

	OneBodyEngine ob_engine(op_type, obs.max_nprim(), obs.max_l());

///  libint2::OneBodyEngine engine(obtype, max_nprim(shells), max_l(shells), 0);

  // nuclear attraction ints engine needs to know where the charges sit ...
  // the nuclei are charges in this case; in QM/MM there will also be classical charges
  if (op_type == libint2::OneBodyEngine::nuclear) {
    std::vector<std::pair<double,std::array<double,3>>> q;
    for(const auto& atom : atoms) {
      q.push_back( {static_cast<double>(atom.atomic_number), {{atom.x, atom.y, atom.z}}} );
    }
    ob_engine.set_params(q);
  }

	// iterate over shells
	for(auto s1=0; s1!=obs.size(); ++s1) {
  	for(auto s2=0; s2!=obs.size(); ++s2) {

//  	  std::cout << "compute shell set {" << s1 << "," << s2 << "} ... ";
  	  const auto* ints_shellset = ob_engine.compute(obs[s1], obs[s2]);
//  	  std::cout << "done" << std::endl;

  	  auto bf1 = shell2bf[s1];  // first basis function in first shell
  	  auto n1 = obs[s1].size(); // number of basis functions in first shell
  	  auto bf2 = shell2bf[s2];  // first basis function in second shell
  	  auto n2 = obs[s2].size(); // number of basis functions in second shell

			// http://eigen.tuxfamily.org/dox/group__TutorialMapClass.html
      // "map" buffer to a const Eigen Matrix, and copy it to the corresponding blocks of the result
      MapMatrix buf_mat(ints_shellset, n1, n2);
      result.block(bf1, bf2, n1, n2) = buf_mat;
      if (s1 != s2) // if s1 >= s2, copy {s1,s2} to the corresponding {s2,s1} block, note the transpose!
      result.block(bf2, bf1, n2, n1) = buf_mat.transpose();
			
  	}
	}
	return result;	
}

Matrix Int1e::compute_overlap(const BasisSet& obs, const AtomVector& atoms)
{
	return compute_obint(obs, atoms, OneBodyEngine::overlap);
}

Matrix Int1e::compute_kinetic(const BasisSet& obs, const AtomVector& atoms)
{
	return compute_obint(obs, atoms, OneBodyEngine::kinetic);
}

Matrix Int1e::compute_nuclear(const BasisSet& obs, const AtomVector& atoms)
{
	return compute_obint(obs, atoms, OneBodyEngine::nuclear);
}

double Int1e::compute_nuclear_repulsion(const AtomVector& atoms)
{
	// compute the nuclear repulsion energy
	auto nuclear_energy = 0.0;
	for (auto i = 0; i < atoms.size(); i++)
		for (auto j = i + 1; j < atoms.size(); j++) {
			auto xij = atoms[i].x - atoms[j].x;
			auto yij = atoms[i].y - atoms[j].y;
			auto zij = atoms[i].z - atoms[j].z;
			auto r2 = xij*xij + yij*yij + zij*zij;
			auto r = sqrt(r2);
			nuclear_energy += atoms[i].atomic_number * atoms[j].atomic_number / r;
	}
	return nuclear_energy;
}
