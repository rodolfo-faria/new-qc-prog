#ifndef LPA_HPP_
#define LPA_HPP_

#include "pop_analysis.hpp"
#include "defs.hpp"

class LPA: public PopulationAnalysis {

	public:
		void compute(MatrixConstRef D, MatrixConstRef S);
};

#endif /* LPA_HPP_ */
