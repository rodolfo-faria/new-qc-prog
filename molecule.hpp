#ifndef MOLECULE_H
#define MOLECULE_H
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <boost/tokenizer.hpp>
#include <boost/format.hpp>

#include <libint2.hpp>

#include "element.hpp"
#include "defs.hpp"

class Molecule {

	private:
		int symmetry_;
		double mass_;
		int natoms_;
		AtomVector& atoms_;

	public:
		Molecule(AtomVector& atoms);
		void add_atom(const Atom& atom);
		int get_natoms();
		Atom get_atom_by_symbol(const std::string& symbol);
		Atom get_atom_by_Z(const int i);
		Atom get_atom(const int index);;
		void print();
};


#endif
