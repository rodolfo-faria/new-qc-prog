#ifndef INT2E_H
#define INT2E_H

#include <cmath>

#include "defs.hpp"

class Int2e {

	public:

		Matrix compute_RG(BasisSet& obs, MatrixConstRef D);
		Matrix compute_UG(const BasisSet& obs, MatrixConstRef D, MatrixConstRef Dsigma);
		Matrix compute_UG2(const BasisSet& obs, MatrixConstRef D1, MatrixConstRef D2);
};
#endif
