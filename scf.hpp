#ifndef SCF_H
#define SCF_H

#include "int1e.hpp"
#include "defs.hpp"

class SCF {

	// Let the derived classes access these vars
	protected:
		int maxiter_;
		double accuracy_;
		bool debug_;
		bool toggle_diis_;
		bool converged_;
		Matrix S_;
		Matrix D_;

	public:

		// constructor
		SCF();
		// copy constructor
		SCF(const SCF& n);

		void set_maxiter(const int maxiter);
		void set_accuracy(const double accuracy);
		int get_maxiter();
		double get_accuracy();
		bool convergence();

		Matrix get_overlap();
		Matrix get_density();

		// scf main routine.
		// calculates the energy of a system with an self-consistent field method
		// return the energy 
		virtual double compute() = 0;

		void print();
		
};
#endif 
