#ifndef RHF_H
#define RHF_H

#include "scf.hpp"
#include "defs.hpp"

#include <libint2/diis.h>

class RHF: public SCF {

	private:
		int nelectron_;
		int ndocc_;
		AtomVector atoms_;
		BasisSet obs_;

	public:
		RHF(const AtomVector& atoms, const BasisSet& obs);

		// SCF cycle 
		double compute();
};
#endif 
