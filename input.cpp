#include "input.hpp"

using namespace std;
using namespace boost;


int Input::read(istream& inputfile)
{
	tokenize_input(inputfile);
	read_tokens_input();
	return 0;
}

/* 
	In this method we read each token from the input file.
	The tokens are stored in a vector for simplicity.
	
*/
int Input::read_tokens_input()
{

	// debug	
//	for (vector<string>::size_type i = 0; i < tokens_.size(); i++) {
//		cout << format("%s\n") % tokens_[i].c_str();
//	}

	string bsname_target = "BasisName";
	string method_target = "Method";
	string multiplicity_target = "Multiplicity";
	string charge_target = "Charge";
	string xyzfile_target = "XYZFile";

	// sanity check
	
	int ni = 0; // necessary input lines

	for (vector<string>::iterator it = tokens_.begin(); it != tokens_.end(); it++) {

		if (upperCase(*it).find(upperCase(method_target)) == std::string::npos) {
			cerr << format("Method of calculation missing\n");
			return -1;
		} else {
			method_ = ((*it).erase(0, method_target.size() + 1)).c_str();
			tokens_.erase(it);
			++ni;
		} 

		if (upperCase(*it).find(upperCase(bsname_target)) == std::string::npos) {
			cerr << format("Basis file name missing\n");
			return -1;
		} else {
			bsname_ = ((*it).erase(0, bsname_target.size() + 1)).c_str();
			++ni;
			tokens_.erase(it);
		}

		if (upperCase(*it).find(upperCase(multiplicity_target)) == std::string::npos) {
			cerr << format("Multiplicity of the system missing\n");
			return -1;
		} else {
			multiplicity_ = atoi(((*it).erase(0, multiplicity_target.size() + 1)).c_str());
			tokens_.erase(it);
			++ni;
		} 

		if (upperCase(*it).find(upperCase(charge_target)) == std::string::npos) {
			cerr << format("Charge of the system missing\n");
			return -1;
		} else {
			charge_ = atoi(((*it).erase(0, charge_target.size() + 1)).c_str());
			tokens_.erase(it);
			++ni;
		}

		if (upperCase(*it).find(upperCase(xyzfile_target)) == std::string::npos) {
			cout << *it << endl;
			cerr << format("XYZ file missing\n");
			return -1;
		} else {
			xyzfile_ = ((*it).erase(0, xyzfile_target.size() + 1)).c_str();
			tokens_.erase(it);
			++ni;
		}

		if (ni == 5)
			break;
	}

	cout << format("!!!!! QC PROG !!!!!\n");
	cout << format("Method of calculation %s\n") % method_;
	cout << format("Basis file name %s\n") % bsname_;
	cout << format("Multiplicity of the system %d\n") % multiplicity_;
	cout << format("Charge of the system %d\n") % charge_;

	return 0;
}

/*
	Tokenize each line of the input file.
	Store it in tokens_
*/
int Input::tokenize_input(istream& inputfile)
{
	string line;

	int nlines = 0;

	while (getline(inputfile, line)) {

		if (line.empty())
			return 0;

		typedef boost::tokenizer<boost::char_separator<char> > Tok;
		boost::char_separator<char> sep(" ", "\t");
		Tok tok(line, sep);
		for (Tok::iterator beg = tok.begin();beg != tok.end();++beg)
			tokens_.push_back(*beg);
		nlines++;
	}
	
	return 0;
}

string Input::upperCase(string input) {
  for (string::iterator it = input.begin(); it != input.end(); ++ it)
    *it = toupper(*it);
  return input;
}

