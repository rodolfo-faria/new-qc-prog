#ifndef POP_ANALYSIS_HPP_
#define POP_ANALYSIS_HPP_

#include "defs.hpp"

class PopulationAnalysis {

	public:
		virtual void compute(MatrixConstRef D, MatrixConstRef S) = 0;
};



#endif /* POP_ANALYSIS_HPP_ */
