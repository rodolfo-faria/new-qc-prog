#include "mpa.hpp"
#include "defs.hpp"

using namespace std;

void MPA::compute(MatrixConstRef D, MatrixConstRef S) {

	Matrix Mulliken = 2.0 * D * S;
	cout << format("\nMulliken Matrix (M):\n") << Mulliken << endl;
	cout << format("\nTrace of (M):%e\n") % (Mulliken.trace());
	return;
}
