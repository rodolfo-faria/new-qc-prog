#include "lpa.hpp"
#include "defs.hpp"

using namespace std;

void LPA::compute(MatrixConstRef D, MatrixConstRef S) {

	Eigen::SelfAdjointEigenSolver<Matrix> solver(S);

	Matrix s_sqrt = solver.eigenvalues().cwiseSqrt().asDiagonal();
	Matrix U      = solver.eigenvectors();
	Matrix S_sqrt = U * s_sqrt * U.transpose();

	Matrix Loewdin = 2.0 * S_sqrt * D * S_sqrt;
	cout << format("\nLoewdin Matrix (L):\n") << Loewdin << endl;
	cout << format("\nTrace of (L):%e\n") % (Loewdin.trace());
	return;
}



