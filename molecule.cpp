#include "molecule.hpp"

using namespace std;
using namespace boost;

Molecule::Molecule(AtomVector& atoms) : atoms_(atoms)
{
	atoms_ = atoms;
	natoms_ = atoms_.size();
}

void Molecule::add_atom(const Atom& atom)
{
}

int Molecule::get_natoms()
{
	return natoms_;
}

Atom Molecule::get_atom_by_symbol(const string& symbol)
{
}

Atom Molecule::get_atom_by_Z(const int Z)
{
}

void Molecule::print()
{
	cout << format("Molecule has %d atoms\n") % natoms_;
	return;
}
