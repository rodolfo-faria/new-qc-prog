#ifndef DENSITY_H
#define DENSITY_H

#include "defs.hpp"

// This object shouldn't exist. It must be part of a more general object.

class BasisValues {
	public:
		double get_density_value(const BasisSet& obs, const Matrix& D, const GridData& gdata);
};
#endif
