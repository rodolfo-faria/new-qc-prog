#include "density.hpp"

double BasisValues::get_density_value(const BasisSet& obs, const Matrix& D, const GridData& gdata)
{
	double rho = 0.0;
	// So why don't we change GridData to have array instead of three doubles ?
	std::array<double, 3> new_position { gdata.x, gdata.y, gdata.z };
	obs.move(new_position);

  auto shell2bf = obs.shell2bf();

  for(auto s1=0; s1!=obs.size(); ++s1) {

		// first basis function in this shell
    auto bf1_first = shell2bf[s1];
 		// get number of basis functions in this shell
		auto n1 = obs[s1].size();

    for(auto s2=0; s2!=obs.size(); ++s2) {

			// first basis function in this shell
      auto bf2_first = shell2bf[s2];
			// get number of basis functions in this shell
      auto n2 = obs[s2].size();

			for(auto f1=0; f1!=n1; ++f1) {
				const auto bf1 = f1 + bf1_first;
				for(auto f2=0; f2!=n2; ++f2) {
					const auto bf2 = f2 + bf2_first;
						rho += D(bf1, bf2) * bf1 * bf2;
				}
			}
		}
	}
	return rho;
}
