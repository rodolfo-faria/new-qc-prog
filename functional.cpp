#include "functional.hpp"

using namespace std;

DFA::DFA(string xfunctional, string cfunctional, bool polarized, bool compute_potential)
{
	xfunctional_ = xfunctional;
	cfunctional_ = cfunctional;
	polarized_ = polarized;
	compute_potential_ = compute_potential;


	if (xfunctional_.compare("LSDAX") == 0) {
		//xfunctional_ingredients = 0;
		Functional_Ptr lsdax(new LSDAX(polarized, compute_potential));
		XFunctionalVector.push_back(lsdax);

	} else if (xfunctional_.compare("B88X") == 0) {

		// rho, gradrho
//		xfunctional_ingredients = 1;
		Functional_Ptr b88(new B88X(polarized, compute_potential));
		XFunctionalVector.push_back(b88);

	} else if (xfunctional_.compare("SlaterX") == 0) {
		Functional_Ptr slaterx(new SlaterX(polarized, compute_potential));
		XFunctionalVector.push_back(slaterx);
	}

	if (cfunctional_.compare("LSDAC") == 0) {
/*
		cfunctional_ingredients = 0;
		Functional_Ptr lsdac(new LSDAC(polarized));
		CFunctionalVector.push_back(lsdac);
*/
	} else if (cfunctional_.compare("VWN5C") == 0) {
		// rho, gradrho
		Functional_Ptr vwnc(new VWN5C(polarized, compute_potential));
		CFunctionalVector.push_back(vwnc);
	}
	return;
}

void DFA::compute(GridData& gdata)
{
	double exd[XFunctionalVector.size()];
	double ecd[CFunctionalVector.size()];

	if (xfunctional_.compare("LSDAX") == 0) {
//		cout << format("%s Exchange Functional LSDA\n") % xfunctional_.c_str();
		exd[0] = XFunctionalVector[0]->compute_energy(gdata);
		gdata.exd = exd[0];
	} else if (xfunctional_.compare("B88X") == 0) {
//		cout << format("%s Exchange Functional B88X\n") % xfunctional_.c_str();
		exd[0] = XFunctionalVector[0]->compute_energy(gdata);
		gdata.exd = exd[0];
	} else if (xfunctional_.compare("SlaterX") == 0) {
		exd[0] = XFunctionalVector[0]->compute_energy(gdata);
		gdata.exd = exd[0];
	}

	if (cfunctional_.compare("VWN5C") == 0) {
		ecd[0] = CFunctionalVector[0]->compute_energy(gdata);
		gdata.ecd = ecd[0];

	} else if (xfunctional_.compare("PW81C") == 0) {
	}

	return;
}

LSDAX::LSDAX(bool polarized, bool compute_potential)
{
	polarized_ = polarized;
	compute_potential_ = compute_potential;
}

inline double LSDAX::compute_energy(GridData& gdata)
{
	//-1.5*pow(3.0/4.0*M_PI, 1.0/3.0) = -0.9305257363491
	double cx = (3.0/2.0)*pow(3.0/(4.0*M_PI),1.0/3.0);
	double rho_alpha13 = pow(gdata.rho_alpha, 1.0/3.0);
	double rho_alpha43 = gdata.rho_alpha*rho_alpha13;
	double exd = 0.0;
	if (compute_potential_)
			gdata.dfdrho_alpha = -3.0*pow(3.0/(4.0*M_PI), 1.0/3.0)*rho_alpha13;
	exd = -cx*rho_alpha43;

	if (polarized_) {
		double rho_beta13 = pow(gdata.rho_alpha, 1.0/3.0);
		double rho_beta43 = gdata.rho_alpha*rho_beta13;
		if (compute_potential_)
			gdata.dfdrho_beta = -3.0*pow(3.0/(4.0*M_PI), 1.0/3.0)*rho_beta13;
		exd += -cx*rho_beta43;
	}

	return exd;
}

SlaterX::SlaterX(bool polarized, bool compute_potential)
{
	polarized_ = polarized;
	compute_potential_ = compute_potential;
	alpha_ = 1.5;
}

inline double SlaterX::compute_energy(GridData& gdata)
{
	//-1.5*pow(3.0/4.0*M_PI, 1.0/3.0) = -0.9305257363491
//	double cx = (9.0/4.0)*alpha_*pow(3.0/(4.0*M_PI), 1.0/3.0);
	double cx = 0.9305257363491;
	double rho_alpha13 = pow(gdata.rho_alpha, 1.0/3.0);
	double rho_alpha43 = gdata.rho_alpha*rho_alpha13;
	double exd = 0.0;

	exd = -cx*rho_alpha43;

	if (compute_potential_)
			gdata.dfdrho_alpha = -3.0*alpha_*pow(3.0/(4.0*M_PI), 1.0/3.0)*rho_alpha13;

	if (polarized_) {
		double rho_beta13 = pow(gdata.rho_beta, 1.0/3.0);
		double rho_beta43 = gdata.rho_beta*rho_beta13;

		if (compute_potential_)
			gdata.dfdrho_beta = -3.0*alpha_*pow(3.0/(4.0*M_PI), 1.0/3.0)*rho_beta13;

		exd += -cx*rho_beta43;
	}

	return exd;
}

B88X::B88X(bool polarized, bool compute_potential)
{
	polarized_ = polarized;
	compute_potential_ = compute_potential;
}

inline double B88X::compute_energy(GridData& gdata)
{
	double exd = 0.0;
	double cx = (3.0/2.0)*pow(3.0/(4.0*M_PI),1.0/3.0);
	double b = 0.0042;
	double b2 = 0.0042*0.0042;
	double rho_alpha13 = pow(gdata.rho_alpha, 1.0/3.0);
	double rho_alpha43 = gdata.rho_alpha*rho_alpha13;

	if (gdata.rho_alpha > 1.0e-14) {
		auto x_alpha = (gdata.gradrho_alpha)/(gdata.rho_alpha * pow(gdata.rho_alpha, 1.0/3.0));
		auto gx_alpha = -((b*pow(x_alpha, 2)/(1.0 + 6.0*b*x_alpha*asinh(x_alpha))));
//		cout << format("x_alpha: %e gx_alpha: %e\n") % x_alpha % gx_alpha;
		exd = -cx*rho_alpha43 + rho_alpha43*gx_alpha;
		if (compute_potential_) {
			auto gprimex_alpha = (6.0*b2*x_alpha*x_alpha*(x_alpha/sqrt(x_alpha*x_alpha + 1) - asinh(x_alpha)) - 2.0*b*x_alpha)/
													pow(1.0 + 6.0*b*x_alpha*asinh(x_alpha), 2);
			gdata.dfdrho_alpha = (4.0/3.0)*rho_alpha43*(gx_alpha - x_alpha*gprimex_alpha);
			gdata.dfdgrad_alphaalpha = 0.5*(pow(gdata.gradrho_alpha, -0.5))*gprimex_alpha;
		}
	}

	if (polarized_) {
		double rho_beta13 = pow(gdata.rho_alpha, 1.0/3.0);
		double rho_beta43 = gdata.rho_alpha*rho_alpha13;

		if (gdata.rho_beta > 1.0e-14) {
			auto x_beta = (gdata.gradrho_beta)/(gdata.rho_beta * pow(gdata.rho_beta, 1.0/3.0));
			auto gx_beta = -((b*pow(x_beta, 2)/(1.0 + 6.0*b*x_beta*asinh(x_beta))));
//			cout << format("x_beta: %e gx_beta: %e\n") % x_beta % gx_beta;
			exd += -cx*rho_beta43 + rho_beta43*gx_beta;

			if (compute_potential_) {
				auto gprimex_beta= (6.0*b2*x_beta*x_beta*(x_beta/sqrt(x_beta*x_beta + 1) - asinh(x_beta)) - 2.0*b*x_beta)/
															pow(1.0 + 6.0*b*x_beta*asinh(x_beta), 2);
				gdata.dfdrho_beta = (4.0/3.0)*rho_beta43*(gx_beta - x_beta*gprimex_beta);
				gdata.dfdgrad_betabeta = 0.5*(pow(gdata.gradrho_beta, -0.5))*gprimex_beta;
			}
		}
	}

	return exd;
}

VWN5C::VWN5C(bool polarized, bool compute_potential)
{
	polarized_ = polarized;
	compute_potential_ = compute_potential;
  Ap_ = 0.0310907;
  Af_ = 0.01554535;
  A_alpha_ = -1./(6.*M_PI*M_PI);

  x0p_mc_ = -0.10498;
  bp_mc_  = 3.72744;
  cp_mc_  = 12.9352;
  x0f_mc_ = -0.32500;
  bf_mc_  = 7.06042;
  cf_mc_  = 18.0578;
  x0_alpha_mc_ = -0.00475840;
  b_alpha_mc_  = 1.13107;
  c_alpha_mc_  = 13.0045;
  
  x0p_rpa_ = -0.409286;
  bp_rpa_  = 13.0720;
  cp_rpa_  = 42.7198;
  x0f_rpa_ = -0.743294;
  bf_rpa_  = 20.1231;
  cf_rpa_  = 101.578;
  x0_alpha_rpa_ = -0.228344;
  b_alpha_rpa_  = 1.06835;
  c_alpha_rpa_  = 11.4813;

}

inline double VWN5C::compute_energy(GridData& gdata)
{

  const double fpp0 = 4./9. * 1./(pow(2., (1./3.)) - 1.);
  const double sixth = 1./6.;
  const double four_thirds = 4./3.;
  const double one_third = 1./3.;
  const double two_thirds = 2./3.;
	double rho = gdata.rho_alpha + gdata.rho_beta;
	double ec = 0.0;

	if (rho > 1.0e-14) {
		double zeta = (gdata.rho_alpha - gdata.rho_beta)/rho;
		double x = pow(3./(4.*M_PI*rho), sixth);
		double rs = x*x;

		double epc    = F(x, Ap_, x0p_mc_, bp_mc_, cp_mc_);
		double efc    = F(x, Af_, x0f_mc_, bf_mc_, cf_mc_);
		double alphac = F(x, A_alpha_, x0_alpha_mc_, b_alpha_mc_, c_alpha_mc_);

		// part of eq 2.3   
		double f = 9./8.*fpp0*(pow(1.+zeta, four_thirds)+pow(1.-zeta, four_thirds)-2.);
		double zeta2 = zeta*zeta;
		double zeta4 = zeta2*zeta2;

		// eq 3.3
		double beta = fpp0 * (efc - epc) / alphac - 1.;

		// eq 3.2
		double delta_ec = alphac * f / fpp0 * (1. + beta * zeta4);

		// eq 2.4
		ec = epc + delta_ec;

		if (compute_potential_) {
		 if (!polarized_) {
			 double depc_dr_s0 = dFdr_s(x, Ap_, x0p_mc_, bp_mc_, cp_mc_); 
			 double dec_dr_s = depc_dr_s0;
			 gdata.dfdrho_alpha = gdata.dfdrho_beta = ec - (rs/3.)*dec_dr_s;
	//		 decrs = dec_dr_s;
	//		 deczeta = 0.;
		 }
		 else {
			 double zeta3 = zeta2*zeta;
			 double depc_dr_s0 = dFdr_s(x, Ap_, x0p_mc_, bp_mc_, cp_mc_);
			 double defc_dr_s1 = dFdr_s(x, Af_, x0f_mc_, bf_mc_, cf_mc_);
			 double dalphac_dr_s = dFdr_s(x, A_alpha_, x0_alpha_mc_, b_alpha_mc_, c_alpha_mc_);
			 double dec_dr_s = depc_dr_s0*(1 - f*zeta4) + defc_dr_s1 * f * zeta4
												 + dalphac_dr_s * f / fpp0 * (1 - zeta4);
			 double fp = two_thirds * (pow((1+zeta),one_third) 
						 - pow((1-zeta),one_third))/(pow(2.,one_third)-1);
			 double dec_dzeta = 4.* zeta3 * f * (efc - epc - (alphac/fpp0))
							 + fp * (zeta4 * (efc - epc) + (1-zeta4)*(alphac/fpp0));
			 gdata.dfdrho_alpha = ec - (rs/3.)*dec_dr_s - (zeta-1)*dec_dzeta;
			 gdata.dfdrho_beta = ec - (rs/3.)*dec_dr_s - (zeta+1)*dec_dzeta;
	//		 decrs = dec_dr_s;
	//		 deczeta = dec_dzeta;
			 } 
		}
	} else {
		ec = 0.0;
		rho = 0.0;
	}

  return (ec * rho);
	
}

double VWN5C::F(double x, double A, double x0, double b, double c)
{
  double x2 = x*x;
  double x02 = x0*x0;
  double Xx = x2 + b*x + c;
  double Xx0 = x02 + b*x0 + c;
  double Q = sqrt(4.*c-b*b);
  double res
      = A * ( log(x2/Xx)
              + 2.*b/Q * atan(Q/(2.*x+b))
              - b*x0/Xx0 * ( log((x-x0)*(x-x0)/Xx)
                           + 2.*(b+2.*x0)/Q * atan(Q/(2.*x+b)) ) );
  return res;
}

double VWN5C::dFdr_s(double x, double A, double x0, double b, double c)
{
  double x2 = x*x;
  double x02 = x0*x0;
  double Xx = x2 + b*x +c;
  double Xx0 = x02 + b*x0 + c;
  double Q = sqrt(4.*c-b*b);
  double res
      = A * ( 1./x2 - 1./Xx - b/(2.*Xx*x) 
          + ((x0*(2.*x0+b))/Xx0 - 1) * (2.*b)/(x*(Q*Q+(2.*x+b)*(2.*x+b))) 
          - (b*x0)/(x*(x-x0)*Xx0) + (b*x0*(1+(b/(2.*x))))/(Xx0*Xx) );
  return res;
}

