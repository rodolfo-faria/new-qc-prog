#ifndef DEFS_H
#define DEFS_H

#include <iostream>
#include <vector>

#include <libint2.hpp>

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/Cholesky>

#include <boost/format.hpp>

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
typedef Eigen::Matrix<double, 1, Eigen::Dynamic> Vector;
typedef Eigen::Map<const Matrix> MapMatrix;
typedef Eigen::Ref<Matrix> MatrixRef;
typedef const Eigen::Ref<const Matrix>& MatrixConstRef;


typedef libint2::BasisSet BasisSet;
typedef libint2::Shell Shell;
typedef libint2::Atom Atom;
typedef libint2::OneBodyEngine::operator_type OperatorType;
typedef libint2::OneBodyEngine OneBodyEngine;
typedef std::vector<libint2::Atom > AtomVector;

typedef boost::format format;

typedef unsigned short ushort;


#endif
