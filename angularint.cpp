#include "angularint.hpp"

using namespace std;

void LebedevLaikovQuadrature::compute(vector<double>& x, vector<double>& y, vector<double>& z, vector<double>& w)
{
	LD0194(x, y, z, w);
	return;
}

void LebedevLaikovQuadrature::GEN_OH(int code, int &num, vector<double>& x, vector<double>& y, vector<double>& z, 
				vector<double>& w, double a, double b, double v)
{
       double  c;
/*     ///  
chvd
chvd   This subroutine is part of a set of subroutines that generate
chvd   Lebedev grids [1-6] for integration on a sphere. The original 
chvd   C-code [1] was kindly provided by Dr. Dmitri N. Laikov and 
chvd   translated into fortran by Dr. Christoph van Wuellen.
chvd   This subroutine was translated from C to fortran77 by hand.
chvd
chvd   Users of this code are asked to include reference [1] in their
chvd   publications, and in the user- and programmers-manuals 
chvd   describing their codes.
chvd
chvd   This code was distributed through CCL (http://www.ccl.net/).
chvd
chvd   [1] V.I. Lebedev, and D.N. Laikov
chvd       "A quadrature formula for the sphere of the 131st
chvd        algebraic order of accuracy"
chvd       Doklady Mathematics, Vol. 59, No. 3, 1999, pp. 477-481.
chvd
chvd   [2] V.I. Lebedev
chvd       "A quadrature formula for the sphere of 59th algebraic
chvd        order of accuracy"
chvd       Russian Acad. Sci. Dokl. Math., Vol. 50, 1995, pp. 283-286. 
chvd
chvd   [3] V.I. Lebedev, and A.L. Skorokhodov
chvd       "Quadrature formulas of orders 41, 47, and 53 for the sphere"
chvd       Russian Acad. Sci. Dokl. Math., Vol. 45, 1992, pp. 587-592. 
chvd
chvd   [4] V.I. Lebedev
chvd       "Spherical quadrature formulas exact to orders 25-29"
chvd       Siberian Mathematical Journal, Vol. 18, 1977, pp. 99-107. 
chvd
chvd   [5] V.I. Lebedev
chvd       "Quadratures on a sphere"
chvd       Computational Mathematics and Mathematical Physics, Vol. 16,
chvd       1976, pp. 10-24. 
chvd
chvd   [6] V.I. Lebedev
chvd       "Values of the nodes and weights of ninth to seventeenth 
chvd        order Gauss-Markov quadrature formulae invariant under the
chvd        octahedron group with inversion"
chvd       Computational Mathematics and Mathematical Physics, Vol. 15,
chvd       1975, pp. 44-51.
chvd
cvw
cvw    Given a point on a sphere (specified by a and b), generate all
cvw    the equivalent points under Oh symmetry, making grid points with
cvw    weight v.
cvw    The variable num is increased by the number of different points
cvw    generated.
cvw
cvw    Depending on code, there are 6...48 different but equivalent
cvw    points.
cvw
cvw    code=1:   (0,0,1) etc                                (  6 points)
cvw    code=2:   (0,a,a) etc, a=1/sqrt(2)                   ( 12 points)
cvw    code=3:   (a,a,a) etc, a=1/sqrt(3)                   (  8 points)
cvw    code=4:   (a,a,b) etc, b=sqrt(1-2 a^2)               ( 24 points)
cvw    code=5:   (a,b,0) etc, b=sqrt(1-a^2), a input        ( 24 points)
cvw    code=6:   (a,b,c) etc, c=sqrt(1-a^2-b^2), a/b input  ( 48 points)
cvw
*/
//       goto (1,2,3,4,5,6) code

//       stop

  switch(code)
  {
    case 1:

       a=1.0;
       x[0 + num] =  a;
       y[0+num] =  0.0;
       z[0+num] =  0.0;
       w[0+num] =  v;
       x[1+num] = -a;
       y[1+num] =  0.0;
       z[1+num] =  0.0;
       w[1+num] =  v;
       x[2+num] =  0.0;
       y[2+num] =  a;
       z[2+num] =  0.0;
       w[2+num] =  v;
       x[3+num] =  0.0;
       y[3+num] = -a;
       z[3+num] =  0.0;
       w[3+num] =  v;
       x[4+num] =  0.0;
       y[4+num] =  0.0;
       z[4+num] =  a;
       w[4+num] =  v;
       x[5+num] =  0.0;
       y[5+num] =  0.0;
       z[5+num] = -a;
       w[5+num] =  v;
       num=num+6;
      break;

    case 2:

       a=sqrt(0.5);
       x[0+num] =  0;
       y[0+num] =  a;
       z[0+num] =  a;
       w[0+num] =  v;
       x[1+num] =  0;
       y[1+num] = -a;
       z[1+num] =  a;
       w[1+num] =  v;
       x[2+num] =  0;
       y[2+num] =  a;
       z[2+num] = -a;
       w[2+num] =  v;
       x[3+num] =  0;
       y[3+num] = -a;
       z[3+num] = -a;
       w[3+num] =  v;
       x[4+num] =  a;
       y[4+num] =  0;
       z[4+num] =  a;
       w[4+num] =  v;
       x[5+num] = -a;
       y[5+num] =  0;
       z[5+num] =  a;
       w[5+num] =  v;
       x[6+num] =  a;
       y[6+num] =  0;
       z[6+num] = -a;
       w[6+num] =  v;
       x[7+num] = -a;
       y[7+num] =  0;
       z[7+num] = -a;
       w[7+num] =  v;
       x[8+num] =  a;
       y[8+num] =  a;
       z[8+num] =  0;
       w[8+num] =  v;
       x[9+num] = -a;
       y[9+num] =  a;
       z[9+num] =  0;
       w[9+num] =  v;
       x[10+num] =  a;
       y[10+num] = -a;
       z[10+num] =  0;
       w[10+num] =  v;
       x[11+num] = -a;
       y[11+num] = -a;
       z[11+num] =  0;
       w[11+num] =  v;
       num=num+12;
      break;

    case 3:

       a = sqrt(1.0/3.0);
       x[0+num] =  a;
       y[0+num] =  a;
       z[0+num] =  a;
       w[0+num] =  v;
       x[1+num] = -a;
       y[1+num] =  a;
       z[1+num] =  a;
       w[1+num] =  v;
       x[2+num] =  a;
       y[2+num] = -a;
       z[2+num] =  a;
       w[2+num] =  v;
       x[3+num] = -a;
       y[3+num] = -a;
       z[3+num] =  a;
       w[3+num] =  v;
       x[4+num] =  a;
       y[4+num] =  a;
       z[4+num] = -a;
       w[4+num] =  v;
       x[5+num] = -a;
       y[5+num] =  a;
       z[5+num] = -a;
       w[5+num] =  v;
       x[6+num] =  a;
       y[6+num] = -a;
       z[6+num] = -a;
       w[6+num] =  v;
       x[7+num] = -a;
       y[7+num] = -a;
       z[7+num] = -a;
       w[7+num] =  v;
       num=num+8;
      break;


    case 4:

       b = sqrt(1 - 2*a*a);
       x[0+num] =  a;
       y[0+num] =  a;
       z[0+num] =  b;
       w[0+num] =  v;
       x[1+num] = -a;
       y[1+num] =  a;
       z[1+num] =  b;
       w[1+num] =  v;
       x[2+num] =  a;
       y[2+num] = -a;
       z[2+num] =  b;
       w[2+num] =  v;
       x[3+num] = -a;
       y[3+num] = -a;
       z[3+num] =  b;
       w[3+num] =  v;
       x[4+num] =  a;
       y[4+num] =  a;
       z[4+num] = -b;
       w[4+num] =  v;
       x[5+num] = -a;
       y[5+num] =  a;
       z[5+num] = -b;
       w[5+num] =  v;
       x[6+num] =  a;
       y[6+num] = -a;
       z[6+num] = -b;
       w[6+num] =  v;
       x[7+num] = -a;
       y[7+num] = -a;
       z[7+num] = -b;
       w[7+num] =  v;
       x[8+num] =  a;
       y[8+num] =  b;
       z[8+num] =  a;
       w[8+num] =  v;
       x[9+num] = -a;
       y[9+num] =  b;
       z[9+num] =  a;
       w[9+num] =  v;
       x[10+num] =  a;
       y[10+num] = -b;
       z[10+num] =  a;
       w[10+num] =  v;
       x[11+num] = -a;
       y[11+num] = -b;
       z[11+num] =  a;
       w[11+num] =  v;
       x[12+num] =  a;
       y[12+num] =  b;
       z[12+num] = -a;
       w[12+num] =  v;
       x[13+num] = -a;
       y[13+num] =  b;
       z[13+num] = -a;
       w[13+num] =  v;
       x[14+num] =  a;
       y[14+num] = -b;
       z[14+num] = -a;
       w[14+num] =  v;
       x[15+num] = -a;
       y[15+num] = -b;
       z[15+num] = -a;
       w[15+num] =  v;
       x[16+num] =  b;
       y[16+num] =  a;
       z[16+num] =  a;
       w[16+num] =  v;
       x[17+num] = -b;
       y[17+num] =  a;
       z[17+num] =  a;
       w[17+num] =  v;
       x[18+num] =  b;
       y[18+num] = -a;
       z[18+num] =  a;
       w[18+num] =  v;
       x[19+num] = -b;
       y[19+num] = -a;
       z[19+num] =  a;
       w[19+num] =  v;
       x[20+num] =  b;
       y[20+num] =  a;
       z[20+num] = -a;
       w[20+num] =  v;
       x[21+num] = -b;
       y[21+num] =  a;
       z[21+num] = -a;
       w[21+num] =  v;
       x[22+num] =  b;
       y[22+num] = -a;
       z[22+num] = -a;
       w[22+num] =  v;
       x[23+num] = -b;
       y[23+num] = -a;
       z[23+num] = -a;
       w[23+num] =  v;
       num=num+24;
      break;

    case 5:

       b=sqrt(1-a*a);
       x[0+num] =  a;
       y[0+num] =  b;
       z[0+num] =  0;
       w[0+num] =  v;
       x[1+num] = -a;
       y[1+num] =  b;
       z[1+num] =  0;
       w[1+num] =  v;
       x[2+num] =  a;
       y[2+num] = -b;
       z[2+num] =  0;
       w[2+num] =  v;
       x[3+num] = -a;
       y[3+num] = -b;
       z[3+num] =  0;
       w[3+num] =  v;
       x[4+num] =  b;
       y[4+num] =  a;
       z[4+num] =  0;
       w[4+num] =  v;
       x[5+num] = -b;
       y[5+num] =  a;
       z[5+num] =  0;
       w[5+num] =  v;
       x[6+num] =  b;
       y[6+num] = -a;
       z[6+num] =  0;
       w[6+num] =  v;
       x[7+num] = -b;
       y[7+num] = -a;
       z[7+num] =  0;
       w[7+num] =  v;
       x[8+num] =  a;
       y[8+num] =  0;
       z[8+num] =  b;
       w[8+num] =  v;
       x[9+num] = -a;
       y[9+num] =  0;
       z[9+num] =  b;
       w[9+num] =  v;
       x[10+num] =  a;
       y[10+num] =  0;
       z[10+num] = -b;
       w[10+num] =  v;
       x[11+num] = -a;
       y[11+num] =  0;
       z[11+num] = -b;
       w[11+num] =  v;
       x[12+num] =  b;
       y[12+num] =  0;
       z[12+num] =  a;
       w[12+num] =  v;
       x[13+num] = -b;
       y[13+num] =  0;
       z[13+num] =  a;
       w[13+num] =  v;
       x[14+num] =  b;
       y[14+num] =  0;
       z[14+num] = -a;
       w[14+num] =  v;
       x[15+num] = -b;
       y[15+num] =  0;
       z[15+num] = -a;
       w[15+num] =  v;
       x[16+num] =  0;
       y[16+num] =  a;
       z[16+num] =  b;
       w[16+num] =  v;
       x[17+num] =  0;
       y[17+num] = -a;
       z[17+num] =  b;
       w[17+num] =  v;
       x[18+num] =  0;
       y[18+num] =  a;
       z[18+num] = -b;
       w[18+num] =  v;
       x[19+num] =  0;
       y[19+num] = -a;
       z[19+num] = -b;
       w[19+num] =  v;
       x[20+num] =  0;
       y[20+num] =  b;
       z[20+num] =  a;
       w[20+num] =  v;
       x[21+num] =  0;
       y[21+num] = -b;
       z[21+num] =  a;
       w[21+num] =  v;
       x[22+num] =  0;
       y[22+num] =  b;
       z[22+num] = -a;
       w[22+num] =  v;
       x[23+num] =  0;
       y[23+num] = -b;
       z[23+num] = -a;
       w[23+num] =  v;
       num=num+24;
     break;

    case 6:
       c=sqrt(1 - a*a - b*b);
       x[0+num] =  a;
       y[0+num] =  b;
       z[0+num] =  c;
       w[0+num] =  v;
       x[1+num] = -a;
       y[1+num] =  b;
       z[1+num] =  c;
       w[1+num] =  v;
       x[2+num] =  a;
       y[2+num] = -b;
       z[2+num] =  c;
       w[2+num] =  v;
       x[3+num] = -a;
       y[3+num] = -b;
       z[3+num] =  c;
       w[3+num] =  v;
       x[4+num] =  a;
       y[4+num] =  b;
       z[4+num] = -c;
       w[4+num] =  v;
       x[5+num] = -a;
       y[5+num] =  b;
       z[5+num] = -c;
       w[5+num] =  v;
       x[6+num] =  a;
       y[6+num] = -b;
       z[6+num] = -c;
       w[6+num] =  v;
       x[7+num] = -a;
       y[7+num] = -b;
       z[7+num] = -c;
       w[7+num] =  v;
       x[8+num] =  a;
       y[8+num] =  c;
       z[8+num] =  b;
       w[8+num] =  v;
       x[9+num] = -a;
       y[9+num] =  c;
       z[9+num] =  b;
       w[9+num] =  v;
       x[10+num] =  a;
       y[10+num] = -c;
       z[10+num] =  b;
       w[10+num] =  v;
       x[11+num] = -a;
       y[11+num] = -c;
       z[11+num] =  b;
       w[11+num] =  v;
       x[12+num] =  a;
       y[12+num] =  c;
       z[12+num] = -b;
       w[12+num] =  v;
       x[13+num] = -a;
       y[13+num] =  c;
       z[13+num] = -b;
       w[13+num] =  v;
       x[14+num] =  a;
       y[14+num] = -c;
       z[14+num] = -b;
       w[14+num] =  v;
       x[15+num] = -a;
       y[15+num] = -c;
       z[15+num] = -b;
       w[15+num] =  v;
       x[16+num] =  b;
       y[16+num] =  a;
       z[16+num] =  c;
       w[16+num] =  v;
       x[17+num] = -b;
       y[17+num] =  a;
       z[17+num] =  c;
       w[17+num] =  v;
       x[18+num] =  b;
       y[18+num] = -a;
       z[18+num] =  c;
       w[18+num] =  v;
       x[19+num] = -b;
       y[19+num] = -a;
       z[19+num] =  c;
       w[19+num] =  v;
       x[20+num] =  b;
       y[20+num] =  a;
       z[20+num] = -c;
       w[20+num] =  v;
       x[21+num] = -b;
       y[21+num] =  a;
       z[21+num] = -c;
       w[21+num] =  v;
       x[22+num] =  b;
       y[22+num] = -a;
       z[22+num] = -c;
       w[22+num] =  v;
       x[23+num] = -b;
       y[23+num] = -a;
       z[23+num] = -c;
       w[23+num] =  v;
       x[24+num] =  b;
       y[24+num] =  c;
       z[24+num] =  a;
       w[24+num] =  v;
       x[25+num] = -b;
       y[25+num] =  c;
       z[25+num] =  a;
       w[25+num] =  v;
       x[26+num] =  b;
       y[26+num] = -c;
       z[26+num] =  a;
       w[26+num] =  v;
       x[27+num] = -b;
       y[27+num] = -c;
       z[27+num] =  a;
       w[27+num] =  v;
       x[28+num] =  b;
       y[28+num] =  c;
       z[28+num] = -a;
       w[28+num] =  v;
       x[29+num] = -b;
       y[29+num] =  c;
       z[29+num] = -a;
       w[29+num] =  v;
       x[30+num] =  b;
       y[30+num] = -c;
       z[30+num] = -a;
       w[30+num] =  v;
       x[31+num] = -b;
       y[31+num] = -c;
       z[31+num] = -a;
       w[31+num] =  v;
       x[32+num] =  c;
       y[32+num] =  a;
       z[32+num] =  b;
       w[32+num] =  v;
       x[33+num] = -c;
       y[33+num] =  a;
       z[33+num] =  b;
       w[33+num] =  v;
       x[34+num] =  c;
       y[34+num] = -a;
       z[34+num] =  b;
       w[34+num] =  v;
       x[35+num] = -c;
       y[35+num] = -a;
       z[35+num] =  b;
       w[35+num] =  v;
       x[36+num] =  c;
       y[36+num] =  a;
       z[36+num] = -b;
       w[36+num] =  v;
       x[37+num] = -c;
       y[37+num] =  a;
       z[37+num] = -b;
       w[37+num] =  v;
       x[38+num] =  c;
       y[38+num] = -a;
       z[38+num] = -b;
       w[38+num] =  v;
       x[39+num] = -c;
       y[39+num] = -a;
       z[39+num] = -b;
       w[39+num] =  v;
       x[40+num] =  c;
       y[40+num] =  b;
       z[40+num] =  a;
       w[40+num] =  v;
       x[41+num] = -c;
       y[41+num] =  b;
       z[41+num] =  a;
       w[41+num] =  v;
       x[42+num] =  c;
       y[42+num] = -b;
       z[42+num] =  a;
       w[42+num] =  v;
       x[43+num] = -c;
       y[43+num] = -b;
       z[43+num] =  a;
       w[43+num] =  v;
       x[44+num] =  c;
       y[44+num] =  b;
       z[44+num] = -a;
       w[44+num] =  v;
       x[45+num] = -c;
       y[45+num] =  b;
       z[45+num] = -a;
       w[45+num] =  v;
       x[46+num] =  c;
       y[46+num] = -b;
       z[46+num] = -a;
       w[46+num] =  v;
       x[47+num] = -c;
       y[47+num] = -b;
       z[47+num] = -a;
       w[47+num] =  v;
       num=num+48;
     break;

    default:
      break;
  }
	return;
}

void LebedevLaikovQuadrature::LD0194(vector<double>& x, vector<double>& y, vector<double>& z, vector<double>& w)
{
	int N;
	double a=0.0,b=0.0,v=0.0;
/*CVW
CVW    LEBEDEV  194-POINT ANGULAR GRID
CVW
chvd
chvd   This subroutine is part of a set of subroutines that generate
chvd   Lebedev grids [1-6] for integration on a sphere. The original
chvd   C-code [1] was kindly provided by Dr. Dmitri N. Laikov and
chvd   translated into fortran by Dr. Christoph van Wuellen.
chvd   This subroutine was translated using a C to fortran77 conversion
chvd   tool written by Dr. Christoph van Wuellen.
chvd
chvd   Users of this code are asked to include reference [1] in their
chvd   publications, and in the user- and programmers-manuals
chvd   describing their codes.
chvd
chvd   This code was distributed through CCL (http://www.ccl.net/).
chvd
chvd   [1] V.I. Lebedev, and D.N. Laikov
chvd       "A quadrature formula for the sphere of the 131st
chvd        algebraic order of accuracy"
chvd       Doklady Mathematics, Vol. 59, No. 3, 1999, pp. 477-481.
chvd
chvd   [2] V.I. Lebedev
chvd       "A quadrature formula for the sphere of 59th algebraic
chvd        order of accuracy"
chvd       Russian Acad. Sci. Dokl. Math., Vol. 50, 1995, pp. 283-286.
chvd
chvd   [3] V.I. Lebedev, and A.L. Skorokhodov
chvd       "Quadrature formulas of orders 41, 47, and 53 for the sphere"
chvd       Russian Acad. Sci. Dokl. Math., Vol. 45, 1992, pp. 587-592.
chvd
chvd   [4] V.I. Lebedev
chvd       "Spherical quadrature formulas exact to orders 25-29"
chvd       Siberian Mathematical Journal, Vol. 18, 1977, pp. 99-107.
chvd
chvd   [5] V.I. Lebedev
chvd       "Quadratures on a sphere"
chvd       Computational Mathematics and Mathematical Physics, Vol. 16,
chvd       1976, pp. 10-24.
chvd
chvd   [6] V.I. Lebedev
chvd       "Values of the nodes and weights of ninth to seventeenth
chvd        order Gauss-Markov quadrature formulae invariant under the
chvd        octahedron group with inversion"
chvd       Computational Mathematics and Mathematical Physics, Vol. 15,
chvd       1975, pp. 44-51.
chvd    */
       N=1;
       v=0.1782340447244611e-2;
       GEN_OH( 1, N, x, y, z, w, a, b, v);
       v=0.5716905949977102e-2;
       GEN_OH( 2, N, x, y, z, w, a, b, v);
       v=0.5573383178848738e-2;
       GEN_OH( 3, N, x, y, z, w, a, b, v);
       a=0.6712973442695226;
       v=0.5608704082587997e-2;
       GEN_OH( 4, N, x, y, z, w, a, b, v);
       a=0.2892465627575439;
       v=0.5158237711805383e-2;
       GEN_OH( 4, N, x, y, z, w, a, b, v);
       a=0.4446933178717437;
       v=0.5518771467273614e-2;
       GEN_OH( 4, N, x, y, z, w, a, b, v);
       a=0.1299335447650067 ;
       v=0.4106777028169394e-2;
       GEN_OH( 4, N, x, y, z, w, a, b, v);
       a=0.3457702197611283;
       v=0.5051846064614808e-2;
       GEN_OH( 5, N, x, y, z, w, a, b, v);
       a=0.1590417105383530;
       b=0.8360360154824589;
       v=0.5530248916233094e-2;
       GEN_OH( 6, N, x, y, z, w, a, b, v);
       N=N-1;
	return;
}

void GaussLegendreQuadrature::compute(double x1, double x2, vector<double>& x, vector<double>& w, int n)
{
  int m,j,i;
  double z1,z,xm,xl,pp,p3,p2,p1;
//  const double EPS = 10.0 * DBL_EPSILON;

  m=(n+1)/2;
  xm=0.5*(x2+x1);
  xl=0.5*(x2-x1);
  for (i=1;i<=m;i++)  {
      z=cos(M_PI*(i-0.25)/(n+0.5));
      do {
          p1=1.0;
          p2=0.0;
          for (j=1;j<=n;j++) {
              p3=p2;
              p2=p1;
              p1=((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j;
	    }
          pp=n*(z*p1-p2)/(z*z-1.0);
          z1=z;
          z=z1-p1/pp;
	} while (fabs(z-z1) > EPS);
      x[i-1]=xm-xl*z;
      x[n-i]=xm+xl*z;
      w[i-1]=2.0*xl/((1.0-z*z)*pp*pp);
      w[n-i]=w[i-1];
    }
}

