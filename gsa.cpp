#include <math.h> // provides the tgamma function
#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include <stdio.h>

#include "gsa.hpp"
#include "defs.hpp"
#include "int1e.hpp"
#include "int2e.hpp"

using namespace std;

#define One 1.0e0

GSA::GSA(const AtomVector& atoms, const BasisSet& obs)
	: atoms_(atoms), obs_(obs),
		qAccentance_(1.1),
	  qTemperature_(1.9),
	  qVisiting_(2.9),
	  maxiter_(100000),
	  accuracy_(1.e-6),
	  dimension_(0.0e0),
	  Tinitial_(50.0e0),
	  nelectron_(0),
	  ndocc_(0),
	  ncycles_(0),
	  converged_(false),
	  guess_hcore(true)
{
}

Matrix GSA::hcore_guess(MatrixRef H) {

	// get the number of basis functions
	const auto nbf = obs_.nbf();

	// count the number of electrons
	for (auto aith: atoms_)
		nelectron_ += aith.atomic_number;

	ndocc_ = nelectron_ / 2;
	cout << format("Number of electrons:%d \nNumber of doubly occupied orbitals:%d\n") % nelectron_ % ndocc_;
	cout << format("Maximum Number of GSA Iterations:%d\nAccuracy:%e\n") % maxiter_ % accuracy_;
	cout << format("Number of basis functions:%d\n\n") % nbf;

	Int1e int1e;

	Matrix S = int1e.compute_overlap(obs_, atoms_);
  cout << format("\n\tOverlap Integrals:\n");
  cout << S << endl;

	Matrix T = int1e.compute_kinetic(obs_, atoms_);
  cout << format("\n\tKinetic Integrals:\n");
  cout << T << endl;

	Matrix V = int1e.compute_nuclear(obs_, atoms_);
	cout << format("\n\tNuclear Integrals:\n");
  cout << V << endl;

  // Core Hamiltonian = T + V
  H = T + V;
  cout << format("\n\tCore Hamiltonian:\n");
  cout << H << endl;

  // T and V no longer needed, free up the memory
  T.resize(0,0);
  V.resize(0,0);

	// solve H C = e S C
	Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver(H, S);
	auto eps = gen_eig_solver.eigenvalues();
	auto C = gen_eig_solver.eigenvectors();

	//compute density, D = C(occ) . C(occ)T
	auto C_occ = C.leftCols(ndocc_);
	Matrix P = C_occ * C_occ.transpose();

	//cout << format("\n\tC_0:\n");
	//cout << C_occ << endl;

	//cout << format("\n\tInitial P:\n");
	//cout << P << endl;

	return C_occ;

}

double GSA::energy() {

	auto final_energy = gsa_engine();
	return final_energy;
}

double GSA::gsa_engine() {

	// acceptance probability
	double qA1 = qAccentance_ - One;
	double Oneqa1 = One/qA1;

	// Temperature
	double qT1 = qTemperature_ - One;
	double Tqt = Tinitial_*(pow(2.0e0,qT1) - One);

	// Visiting Probability
	double qv1 = qVisiting_ - One;
	double qv2 = pow(2.0e0,qv1) - One;

	double tmp 				= One/qv1 - 0.5e0;
	double gama_down 	= tgamma(tmp);
	double exponent1  = 2.0e0/(3.0e0 - qVisiting_);

	double coef1;
	double gama_up;
	double exponent2;

	if (dimension_ == 0.0e0) {
		cout << "dimension: " << dimension_ << endl;
		coef1  			= 1.0e0;
		exponent2   = 1.0e0/qv1 - 0.5e0;
		gama_up			= gama_down;
	} else {

		coef1	 			= pow(qv1/M_PI,dimension_*0.5e0);
		exponent2   = 1.0e0/qv1 + 0.5e0*dimension_ - 0.5e0;
		gama_up 		= tgamma(exponent2);
	}

	double coef = coef1*gama_up/gama_down;

	// get the number of basis functions
	const auto nbf = obs_.nbf();

	Matrix C_0;
	Matrix H = Matrix::Zero(nbf, nbf);

	double energy_min = 0.0e0;
	double energy_t   = 0.0e0;
	double energy_0   = 0.0e0;

	if (not guess_hcore) {

		C_0 = Matrix::Random(nbf, ndocc_);

	} else {

		C_0        = hcore_guess(H);
		Matrix P   = C_0 * C_0.transpose();
		energy_0   = P.cwiseProduct(H).sum();
		energy_t   = energy_0;
		energy_min = energy_0;
	}

	cout << "\nH core guess energy (energy_0): " << energy_0 << endl;
	cout << "\nInitial C_0:\n" << C_0 << endl;

	Matrix C_min = C_0;
	Matrix C_t   = C_0;

	std::srand((unsigned)std::time(0));

	double time 			 = 0.0e0;
	double temperature = 0.0e0;
	double Tup				 = 0.0e0;

	cout << "\ncycles\tEnergy\n";

	Int2e int2e;
	Int1e int1e;

	do
	{
		++ncycles_;

		time += 1.0e0;

		temperature = Tqt/(pow(One + time, qT1) - One); // temperature T(t)

		if (dimension_ == 0.0e0) {
			Tup = One;
		} else {
			Tup = pow(temperature, dimension_/(qVisiting_ - 3.0e0));
		}

		// compute new coefficients using the g(qv,t) function
		g_function(temperature, Tup, C_0, C_t, coef, exponent1, exponent2, qv1);

		// compute RHF energy
		energy_t = rhf_energy(C_t, H, int2e);

		// keep current parameters if the energy is going down
		if (energy_t <= energy_0) {
			C_0 		 = C_t;
			energy_0 = energy_t;

			if (energy_t <= energy_min) {
				C_min 		 = C_t;
				energy_min = energy_t;
				cout << ncycles_<< "\t" << energy_min << endl;
			}
		} else {
				// acceptance probability function [0,1]
				double delta_energy = energy_t - energy_0;
				double Pqa 					= One / pow(One + qA1 * delta_energy / temperature, Oneqa1);
				double random 			= ((double) rand() / (double)(RAND_MAX));

				if (random < Pqa) {
					C_0      = C_t;
					energy_0 = energy_t;
				}
		}

	} while (ncycles_ <= maxiter_ || fabs(energy_t - energy_min) < accuracy_);

	// convergence check
	if (ncycles_ == maxiter_) {
		converged_ = false;
		cerr << "\n\n\t*** GSA cycle not converged! ***\n";
	} else {
		converged_ = true;
	}

	double nuclear_energy = int1e.compute_nuclear_repulsion(atoms_);

	cout << format("Final Electronic Energy: %8.12e\n") % energy_min;

	return (energy_min + nuclear_energy);
}

void GSA::g_function(double temperature, double Tup, MatrixConstRef C_0, MatrixRef C_t,
										double coef, double exponent1, double exponent2, double qv1) {

  double R = 0.0e0;
  double S = 0.0e0;

  double deltaC = 0.0e0;

	for (int j = 0; j < C_0.cols(); j++) {
		for (int i = 0; i < C_0.rows(); i++) {

			S = ((double) rand() / (double)(RAND_MAX));
			R = ((double) rand() / (double)(RAND_MAX));

			double tmp = One + qv1 * R * R / pow(temperature, exponent1);
   		deltaC = coef * Tup / pow(tmp,  exponent2);

			if (S <= 0.5e0) deltaC = -deltaC;

			C_t(i,j) = C_0(i,j) + deltaC;
		}
	}


	/* This class performs a QR decomposition
	 * of a matrix A into matrices Q and R such that
	 * 		A = Q*R
	 * by using Householder transformations.
	 * Here, Q a unitary(orthogonal) matrix
	 * and R an upper triangular matrix.
	 * Note that we need the thinQ, not the fullQ.
	*/
	Matrix thin = Matrix::Identity(C_t.rows(),C_t.cols());
	Eigen::HouseholderQR<Matrix> qr(C_t);
	C_t = qr.householderQ() * thin;

}

double GSA::rhf_energy(MatrixConstRef C, MatrixConstRef H, Int2e& int2e) {

	// compute density, P = C(occ) . C(occ)T
	auto P = C * C.transpose();

	// compute Fock
	auto F = H + int2e.compute_RG(obs_, P);

	return (P.cwiseProduct(H+F).sum());
}

