#ifndef RADIALQUADRATURE_H
#define RADIALQUADRATURE_H

#include "defs.hpp"
#include <vector>
#include <cmath>

#define EPS 3.0e-11

class RadialQuadrature {

	protected:
		// Number of radial points
		int nrad_;
	public:
		RadialQuadrature(const int nrad) : nrad_(nrad) {};
		virtual double compute(int irad, int n, double radii) = 0;

};

class EulerMaclaurinQuadrature: public RadialQuadrature {

	public:
		EulerMaclaurinQuadrature(const int nrad) : RadialQuadrature(nrad) {};
		double compute(int irad, int n, double radii);

};

#endif
