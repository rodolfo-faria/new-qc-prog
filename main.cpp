#include <iostream>
#include <iterator>
#include <fstream>
#include <string>
#include <algorithm>
#include <cstring>
#include <array>
#include <Eigen/Cholesky>

#include "defs.hpp"
#include "input.hpp"
#include "scf.hpp"
#include "rhf.hpp"
#include "uhf.hpp"
#include "uks.hpp"
#include "gsa.hpp"
#include "energy.hpp"
#include "pop_analysis.hpp"
#include "mpa.hpp"
#include "lpa.hpp"

#include <boost/format.hpp>
#include <boost/program_options.hpp>

namespace bpo = boost::program_options;

using namespace std;
using namespace boost;

int start(char *filename, int n_procs)
{
	libint2::init();
	Input input;

	ifstream inputfile(filename);
	input.read(inputfile);

	ifstream lint2_input(input.get_xyzfile());
	AtomVector atoms = libint2::read_dotxyz(lint2_input);
	BasisSet obs(input.get_bsname(), atoms);
	
	auto mul = input.get_multiplicity();
	auto charge = input.get_charge();
	auto nelectrons = 0;

	for (auto aith: atoms)
		nelectrons += aith.atomic_number;

	// set the right number of electrons by applying the charge
	nelectrons -= input.get_charge();
	auto ndocc = nelectrons/2;

	// check multiplicity, number of electrons and charge
	if (mul == 1 && input.get_method() == "RHF") {
		SCF *scf_vector = new RHF(atoms, obs);
		auto energy = scf_vector->compute();
		cout << format("\tRHF SCF Vector energy: %8.12e\n") % energy;

		if (scf_vector->convergence()) {
			Matrix S = scf_vector->get_overlap();
			Matrix D = scf_vector->get_density();
			PopulationAnalysis *pa_mul = new MPA;
			PopulationAnalysis *pa_low = new LPA;
			pa_mul->compute(D, S);
			pa_low->compute(D, S);
		}

		// Call GSA RHF
		Energy *gsa_rhf = new GSA(atoms, obs);
		auto gsa_energy = gsa_rhf->energy();
		cout << format("\tGSA RHF energy: %8.12e\n") % gsa_energy;

	} else if (mul > 1 && input.get_method() == "UHF") {

		// check multiplicity's parity to determine the number of alpha and beta electrons
		unsigned int nalpha;
		unsigned int nbeta;

		if (mul % 2)
			nalpha = nelectrons/2 + (mul - 1)/2;
		else
			nalpha = nelectrons/2 + mul/2;

		nbeta = nelectrons - nalpha;
		cout << format("\tUHF: number of alpha electrons %d\n\tUHF: number of beta electrons %d\n") % nalpha % nbeta;
		SCF *scf_vector = new UHF(atoms, obs, nalpha, nbeta);
		auto energy = scf_vector->compute();
		cout << format("\tUHF SCF Vector energy: %8.12e\n") % energy;

	} else if (mul > 1 && input.get_method() == "UKS") {
		// check multiplicity's parity to determine the number of alpha and beta electrons
		unsigned int nalpha;
		unsigned int nbeta;

		if (mul % 2)
			nalpha = nelectrons/2 + (mul - 1)/2;
		else
			nalpha = nelectrons/2 + mul/2;

		nbeta = nelectrons - nalpha;
		cout << format("\tUHF: number of alpha electrons %d\n\tUHF: number of beta electrons %d\n") % nalpha % nbeta;
		SCF *scf_vector = new UKS(atoms, obs, nalpha, nbeta);
		auto energy = scf_vector->compute();
		cout << format("\tUKS SCF Vector energy: %8.12e\n") % energy;

	} else {
		cerr << format("Error: wrong multiplicity for this method\n");
	  inputfile.close();
	  lint2_input.close();
	  libint2::finalize();
		return -1;
	}

	//test_grid(atoms);

  inputfile.close();
  lint2_input.close();
  libint2::finalize();

  return 0;
}
	
int main(int argc, char** argv)
{
	// number of processors
	unsigned int n_procs;

	// parse command line aguments
//	try {

		bpo::options_description desc("List of arguments");
		desc.add_options()
			("help,h", "print this help message")
			("version,v", "print qc-prog version")
			("np,n", bpo::value<unsigned int >(&n_procs)->default_value(1), "set number of processors for parallel execution")
			("input-file,i", bpo::value<string>(), "input file")
		;

		bpo::variables_map vm;
		bpo::store(bpo::parse_command_line(argc, argv, desc), vm);
		bpo::notify(vm);


		if (vm.count("help") || argc == 1) {
			cout << "\nusage: ./qc-prog -n <nprocs> inpufile\n" << endl;
		    cout << desc << "\n";
		    return 0;
		}

		if (vm.count("version")) {

			cout << "  --------------------------------------- \n";
			cout << "  |          qc-prog version 0.1        | \n";
			cout << "  |                2015                 | \n";
			cout << "  | Rodrigo Bogossian and Rodolfo Faria | \n";
			cout << "  --------------------------------------- \n";

			return 0;
		}

		if (vm.count("input-file")) {

			string input =  vm["input-file"].as< string >();

			char inp[20];
			strcpy(inp,input.c_str());
			char* inputfile = inp;

			start(inputfile, n_procs); // start program execution

//		}
//		else {
//			cout << "\nusage: ./qc-prog -n <nprocs> inpufile\n" << endl;
//			cout << desc << "\n";
//		}

	}
//	catch(...) {
//		cerr << "Exception of unknown type!\n";
//	}

	return 0;

}
