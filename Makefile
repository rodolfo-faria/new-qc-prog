CXX=icpc
CXXFLAGS=-std=c++11 -g -I/usr/local/include/ -I/usr/local/include/libint2 -I/usr/local/libint/2.1.0-beta2/include -I/usr/local/libint/2.1.0-beta2/include/libint2 -I/usr/include/eigen3 -I/usr/local/include/boost -I/usr/local/libint/2.1.0-beta/include -I/usr/local/libint/2.1.0-beta/include/libint2
LDFLAGS=-lint2 -lboost_program_options
LDLIBS=-L/usr/local/libint/2.1.0-beta2/lib -L/usr/local/libint/2.1.0-beta/lib

qc-prog: main.o input.o int1e.o int2e.o scf.o rhf.o uhf.o uks.o element.o molecule.o grid.o radialint.o angularint.o pop_analysis.o mpa.o lpa.o gsa.o energy.o functional.o
	$(CXX) main.o input.o int1e.o int2e.o scf.o rhf.o uhf.o uks.o element.o molecule.o grid.o radialint.o angularint.o pop_analysis.o mpa.o lpa.o gsa.o energy.o functional.o $(LDFLAGS) $(LDLIBS) -o qc-prog
	
input.o: input.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c input.cpp
int1e.o: int1e.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c int1e.cpp
int2e.o: int2e.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c int2e.cpp
scf.o: scf.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c scf.cpp
rhf.o: rhf.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c rhf.cpp
uhf.o: uhf.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c uhf.cpp
uks.o: uks.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c uks.cpp
element.o: element.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c element.cpp
molecule.o: molecule.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c molecule.cpp
grid.o: grid.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c grid.cpp
radialint.o: radialint.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c radialint.cpp
angularint.o: angularint.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c angularint.cpp
pop_analysis.o: pop_analysis.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c pop_analysis.cpp
mpa.o: mpa.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c mpa.cpp
lpa.o: lpa.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c lpa.cpp
gsa.o: gsa.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c gsa.cpp
energy.o: energy.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c energy.cpp
functional.o: functional.cpp 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c functional.cpp
main.o: main.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -c main.cpp


clean:
	rm qc-prog *.o
	
