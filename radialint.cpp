#include "radialint.hpp"

using namespace std;

double EulerMaclaurinQuadrature::compute(int irad, int n, double radii)
{
	
	auto q = (double) irad / (double) n;
	auto v = q/(1.0 - q);
	return (v * v * radii);
}
