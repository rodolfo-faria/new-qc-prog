#include "grid.hpp"

using namespace std;


double BeckeWeight::Distance(const Atom& a, const std::array<double,3>& b)
{
	double distx = 0.0;
	distx = sqrt(pow(a.x - b[0], 2) + pow(a.y - b[1], 2) + pow(a.z - b[2], 2));
	return distx;
}

double BeckeWeight::Distance(const Atom& a, const Atom& b)
{
	double distx = 0.0;
	distx = sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2) + pow(a.z - b.z, 2));
	return distx;
}

bool BeckeWeight::Isatomequal(const Atom& a, const Atom&b)
{
	bool result = false;
	
	if (a.x == b.x && a.y == b.y && a.z == b.z)
		result = true;

	return result;
}

// eq 13
double BeckeWeight::Cell_Function(const Atom& ajth, const GridData& gdata) 
{
	double P;

	// distance from reference point to first center RA
	auto r_RA = Distance(ajth, gdata.point);

	for (auto akth: atoms_) {
		P = 1.0;
		if (!(Isatomequal(ajth, akth))) {

			// eq. a.5 gives aij
			auto aij = Hyperboloid(ajth, akth);

			// distance from reference point to first center RA
			auto r_RB = Distance(akth, gdata.point);

			auto RAB = Distance(ajth, akth);
	
			// eq. 11 gives uij
			auto uij = (r_RA - r_RB)/RAB;
			auto vij = uij + aij*(1.0 - uij*uij);
			P *= Step_Function(vij);
/*
			// test range of the surface
			// eq. 12
			if (uij < -1.0)
				continue;
			else if (uij > 1.0) {
				P = 0.0;
				break;
			} else {
				// surface in range, transform uij to vij
				auto vij = uij + aij*(1.0 - uij*uij);
				// Calculate eq.13 (Pi) with s(vij)
				P *= Step_Function(vij);
			}
*/
		}
	}
	return P;
}

// equation 11
// equations A.4 A.5 and A.6
// check appendix too
double BeckeWeight::Hyperboloid(const Atom& a, const Atom& b)
{
	// eq. A4
	double chi = (BraggRadius[a.atomic_number]/BraggRadius[b.atomic_number])*(1.0/0.52918);
	// eq. A6
	double Uab = (chi - 1.0)/(chi + 1.0);
	// eq. A5
	double Aab = Uab/(Uab*Uab - 1.0);

	return Aab;
	
}

// equation 19
// TTP - Two-Term Polynomial
double BeckeWeight::TTP(double uij)
{
	double p = 0.0;

	p = 1.5*uij - 0.5*pow(uij, 3);
	return p;
}

// equation 20
double BeckeWeight::F3(double uij)
{
	double value = TTP(TTP(TTP(uij)));
	return value;
}

// equation 21
double BeckeWeight::Step_Function(double vij)
{
	double s = 0.0;
	s = 0.5*(1.0 - F3(vij));
	return s;
}

// eq 22
double BeckeWeight::Weight(const Atom& aith, const GridData& gdata)
{
	auto Psum = 0.0;
	auto Pn = 0.0;

	for (auto ajth: atoms_) {

		// Paith = Pn in Becke's formula. The cell function of the ith atom.
		auto Pajth = Cell_Function(ajth, gdata);
//		cout << format("Pajth:%e\n") % Pajth;
		
			
		// when the outer loop in the Build has the same atom as we have here then
		// assign the cell function of this atom to Pn and accumulate it.
		// This is needed because we have three loops over the vector of atoms.
		if (Isatomequal(ajth, aith)) Pn = Pajth;

		// this is the denominator of eq. 22	
		Psum += Pajth;
	}
	// ok do eq. 22
	return (Pn/Psum);
}

// eq 25
void BeckeWeight::RM(const int atomic_number, double& radx)
{	
	// last constant is conversion from Angstrom to Bohr
	double rm = 0.5*BraggRadius[atomic_number]*(1.0/0.52918);
	radx = rm * (1.0 + radx)/(1.0 - radx);
	return;
}

///////////////////////////////////////////////////////////////////////////////


vector<GridData> Grid::compute()
{
	vector<GridData> gdvector;
	auto nelectrons = 0.0;
	auto exd = 0.0;
	auto Ex = 0.0;
	auto Ec = 0.0;

	DFA functionals("SlaterX", "VWN5C", polarized_, false);

	// Initialize points for radial part
	double radx = 0.0;

	// This will be an option in the future
	BeckeWeight BW(atoms_);

	// the number of radial points should be inside the ABC RadialQuadrature
	RadialQuadrature *RQ = new EulerMaclaurinQuadrature(nrad_);

	GaussLegendreQuadrature AQ;
	// Initialize points and weights for angular part
	vector<double> theta_quad_points(16, 0);
	vector<double> theta_quad_weights(16, 0);

	for (auto aith: atoms_) {

		// number of radial points
		for (auto irad = 0;irad < nrad_;irad++) {

			// get the points and weights for 50 radial points
			// the range is between -1 and 1
			// FIX: THIS IS NOT A GENERAL IMPLEMENTATION OF COMPUTE
			radx = RQ->compute(irad, nrad_, BraggRadius[aith.atomic_number]*(1.0/0.52918));

			// for each radial point convert the radial point using becke's map
			// this is only used with legendre's grid since this was designed
			// by becke
			//RM(aith.atomic_number, radx);
			// This is element of the radial integral. The derivative of eq. 25
			//auto dr = 2.0*BraggRadius[aith.atomic_number]/pow(radx, 2);

			auto q = (double)irad/(double)nrad_;
		  auto dr_dq =  2.0*BraggRadius[aith.atomic_number]*(1.0/0.52918)*q*pow(1.0 - q, -3.0);
		  auto drdq_r2 = dr_dq*radx*radx;
		  auto radial_multiplier = drdq_r2/nrad_;

			auto ntheta = 0;
			auto nphi = 0;
			
			if (irad == 0) {
				ntheta = 1;
				nphi = 1;
			} else {
				ntheta = (int) (radx/BraggRadius[aith.atomic_number]*(1.0/0.52918)*5*16);
				if (ntheta > 16)
					ntheta = 16;
				if (ntheta < 6)
					ntheta = 6;
				nphi = 2*ntheta;
			}
	
			AQ.compute(0.0, M_PI, theta_quad_points, theta_quad_weights, ntheta);

			for (auto iang_theta = 0;iang_theta < ntheta;iang_theta++) {

				double theta = theta_quad_points[iang_theta];
				double sin_theta = sin(theta);
				double integral_volume = drdq_r2 * sin_theta;

				for (int iang_phi = 0;iang_phi < nphi;iang_phi++) {

					GridData gdata;
					gdata.rho_alpha = 0.0;
					gdata.rho_beta = 0.0;
					gdata.gradrho_alpha = 0.0;
					gdata.gradrho_beta = 0.0;
					gdata.exd = 0.0;
					gdata.ecd = 0.0;
					double phi = (double) iang_phi/ (double) nphi * 2.0 * M_PI;
					// at this point we have radx, theta and phi
					// conver spherical to cartesian
					
  				double rsin_theta = radx*sin(theta);
					gdata.point[0] = rsin_theta*cos(phi) + aith.x;
					gdata.point[1] = rsin_theta*sin(phi) + aith.y;
					gdata.point[2] = radx*cos(theta) + aith.z;

					gdata.weight = BW.Weight(aith, gdata);

					auto multiplier = gdata.weight * integral_volume * theta_quad_weights[iang_theta]/nrad_ * 2.0 * M_PI/((double)nphi);

					// calculate density and its derivatives 
					// at this point, gdata contains all ingredients used in functionals
					compute_dnd(gdata);
					gdvector.push_back(gdata);

					// evaluate functional at each point
					functionals.compute(gdata);
					
					Ex += multiplier*gdata.exd;
					Ec += multiplier*gdata.ecd;

					// this integrates the density and obviously gives the number of eletrons
					nelectrons += multiplier*(gdata.rho_alpha + gdata.rho_beta);
				} // iang_phi
			}	// iang_theta
		}	// irad
	}	// aith

	cout << format("Number of electrons: %e\n") % nelectrons;
	cout << format("Exchange Energy: %e\nCorrelation Energy: %e\n") % Ex % Ec;

	return gdvector;
}


// Compute density and derivatives (dnd)
inline void Grid::compute_dnd(GridData& gdata)
{	
	double rho_alpha = 0.0;
	double rho_beta = 0.0;
	std::array<double, 3> gradrho_alpha = {0,0,0};
	std::array<double, 3> gradrho_beta = {0,0,0};
	double laprho_alpha = 0.0;
	double laprho_beta = 0.0;
	double bihrho_alpha = 0.0;
	double bihrho_beta = 0.0;
	double tau_alpha = 0.0;
	double tau_beta = 0.0;

	if (polarized_) {

		compute_density(gdata, P_alpha_, rho_alpha, gradrho_alpha, laprho_alpha, bihrho_alpha, tau_alpha);
		compute_density(gdata, P_beta_, rho_beta, gradrho_beta, laprho_beta, bihrho_beta, tau_beta);

		gdata.rho_alpha = rho_alpha;
		gdata.rho_beta = rho_beta;
		gdata.rho = rho_alpha + rho_beta;

		gdata.gradrho_alpha = sqrt(gradrho_alpha[0]*gradrho_alpha[0]+gradrho_alpha[1]*gradrho_alpha[1]+gradrho_alpha[2]*gradrho_alpha[2]);
		gdata.gradrho_beta = sqrt(gradrho_beta[0]*gradrho_beta[0]+gradrho_beta[1]*gradrho_beta[1]+gradrho_beta[2]*gradrho_beta[2]);
		gdata.gradrho = (gradrho_alpha[0]+gradrho_alpha[1]+gradrho_alpha[2]) + (gradrho_beta[0]+gradrho_beta[1]+gradrho_beta[2]);
//		cout << format("rho_alpha: %e rho_beta: %e\n") % gdata.rho_alpha % gdata.rho_beta;
//		cout << format("gradrho_alpha: %e gradrho_beta: %e\n") % gdata.gradrho_alpha % gdata.gradrho_beta;

	} else {
		// if unpolarized P_alpha_ is the total DM
		cout << format("UNPOLARIZED\n");
		compute_density(gdata, P_alpha_, rho_alpha, gradrho_alpha, laprho_alpha, bihrho_alpha, tau_alpha);
		gdata.rho = rho_alpha;
		gdata.gradrho = 2.0*(gradrho_alpha[0]+gradrho_alpha[1]+gradrho_alpha[2]);
	
	}

	return;
}

inline void Grid::compute_density(GridData& gdata, const Matrix& P, double& rho, std::array<double, 3>& gradrho, double& laprho, double& bihrho, double& tau)
{

//	cout << format("%e %e %e\n") % gdata.point[0] % gdata.point[1] % gdata.point[2];
	// move the shells to new point in the grid
	// THIS IS NOT WORKING
//	for (auto& ith: obs_)
//		ith.move(gdata.point);
	
  auto shell2bf = obs_.shell2bf();

  for(auto s1=0; s1!=obs_.size(); ++s1) {

		// first basis function in this shell
    auto bf1_first = shell2bf[s1];

 		// get number of basis functions in this shell
		auto n1 = obs_[s1].size();

		// calculate the basis values here
		auto bv1 = 0.0;
		std::array<double,4> dxphi1 = {0,0,0,0};
		std::array<double,4> dyphi1 = {0,0,0,0};
		std::array<double,4> dzphi1 = {0,0,0,0};

		basis_values(gdata, obs_[s1], bv1, dxphi1, dyphi1, dzphi1);

    for(auto s2=0; s2!=obs_.size(); ++s2) {

			// first basis function in this shell
      auto bf2_first = shell2bf[s2];
			// get number of basis functions in this shell
      auto n2 = obs_[s2].size();

			// calculate the basis values here
			auto bv2 = 0.0;	
			std::array<double,4> dxphi2 = {0,0,0,0};
			std::array<double,4> dyphi2 = {0,0,0,0};
			std::array<double,4> dzphi2 = {0,0,0,0};
			basis_values(gdata, obs_[s2], bv2, dxphi2, dyphi2, dzphi2);
//			cout << format("dxphi1[0]: %e dxphi2[0]: %e\n") % dxphi1[0] % dxphi2[0];

			// n1 contains the number of basis function (gaussians) in this shell (s1)
			for(auto f1=0; f1!=n1; ++f1) {
				// f1 points to the next function in the shell
				// it uses the base address of bf1_first which is the first basis function (gaussian) of this shell
				const auto bf1 = f1 + bf1_first;
				for(auto f2=0; f2!=n2; ++f2) {
					const auto bf2 = f2 + bf2_first;
					rho += P(bf1, bf2) * bv1 * bv2;
					gradrho[0] += P(bf1, bf2) * (bv1 * dxphi1[0] + bv2 * dxphi2[0]);
					gradrho[1] += P(bf1, bf2) * (bv1 * dyphi1[0] + bv2 * dyphi2[0]);
					gradrho[2] += P(bf1, bf2) * (bv1 * dzphi1[0] + bv2 * dzphi2[0]);
				}
			}
		}
	}
//	cout << format("Rho: %e at %e %e %e\n") % rho % gdata.point[0] % gdata.point[1] % gdata.point[2];
	return;
}

inline void Grid::basis_values(GridData& gdata, Shell& s1, double& bv, std::array<double, 4>& dxphi, std::array<double, 4>& dyphi, std::array<double, 4>& dzphi)
{
	int count = 1;
	int dnd = 1;
	auto nprim_s1 = s1.nprim();
	auto ncontr_s1 = s1.ncontr();
	int xc[nprim_s1];
	int yc[nprim_s1];
	int zc[nprim_s1];

	// Center of this shell
	auto& A = s1.O;

	auto RAx = (gdata.point[0] - A[0]);
	auto RAy = (gdata.point[1] - A[1]);
	auto RAz = (gdata.point[2] - A[2]);

	int am1 = s1.contr[0].l;

	for (int i = 0;i < nprim_s1;i++) {
		xc[i] = 0;
		yc[i] = 0;
		zc[i] = 0;
	}
	
	for (int i = 0;i < ncontr_s1;i++) {

		for (int j = 0;j < nprim_s1;j++) {

				// access alpha's and contraction coefficients
				auto alpha = s1.alpha[j];
				auto coeff = s1.contr[0].coeff[j];
				auto alpha_r2 = alpha * (RAx*RAx + RAy*RAy + RAz*RAz);
//				cout << format("xc: %d yc: %d zc: %d\n") % xc[j] % yc[j] % zc[j];
//				cout << format("RAx: %e RAy: %e RAz: %e\n") % RAx % RAy % RAz;
//				cout << format("alpha: %e coeff: %e r2: %e\n") % alpha1 % coeff1 % ((RAx*RAx + RAy*RAy + RAz*RAz));
//
//
				if (am1 != 0) {	
					for (int k1 = 0; k1 <= am1; ++k1) {
						for (int n1 = 0; n1 <= k1; ++n1) {
							const int m1 = k1 - n1;
							const int l1 = am1 - k1;

							xc[j] = l1;
							yc[j] = m1;
							zc[j] = n1;
						}
					}
				}

				auto ith_gf = pow(RAx, xc[j]) * pow(RAy, yc[j]) * pow(RAz, zc[j]) * exp(- alpha_r2);

				bv += coeff*ith_gf;

				auto exp_alpha_r2 = exp(-alpha*(pow(RAx, 2) + pow(RAy, 2) + pow(RAz, 2)));

				if (am1 == 0) {
					dxphi[0] += coeff*(-2.0*alpha*RAx*exp_alpha_r2);
					dxphi[1] += coeff*(2.0*alpha*(2.0*pow(RAx,2.0)*alpha - 1.0)*exp_alpha_r2);
					dxphi[2] += coeff*(-2.0*pow(alpha,2.0)*RAx*(2.0*pow(RAx,2.0)*alpha - 3.0)*exp_alpha_r2);
					dxphi[3] += coeff*(pow(alpha,2)*(16.0*pow(RAx, 4.0)*pow(alpha, 2.0) - 48.0*pow(RAx,2)*alpha + 12.0)*exp_alpha_r2);
					// DY
					dyphi[0] += coeff*(-2.0*alpha*RAy*exp_alpha_r2);
					dyphi[1] += coeff*(2.0*alpha*(2.0*pow(RAy,2.0)*alpha - 1.0)*exp_alpha_r2);
					dyphi[2] += coeff*(-2.0*pow(alpha,2.0)*RAy*(2.0*pow(RAy,2.0)*alpha - 3.0)*exp_alpha_r2);
					dyphi[3] += coeff*(pow(alpha,2)*(16.0*pow(RAy, 4.0)*pow(alpha, 2.0) - 48.0*pow(RAy,2)*alpha + 12.0)*exp_alpha_r2);

					// DZ
					dzphi[0] += coeff*(-2.0*alpha*RAz*exp_alpha_r2);
					dzphi[1] += coeff*(2.0*alpha*(2.0*pow(RAz,2.0)*alpha - 1.0)*exp_alpha_r2);
					dzphi[2] += coeff*(-2.0*pow(alpha,2.0)*RAz*(2.0*pow(RAz,2.0)*alpha - 3.0)*exp_alpha_r2);
					dzphi[3] += coeff*(pow(alpha,2)*(16.0*pow(RAz, 4.0)*pow(alpha, 2.0) - 48.0*pow(RAz,2)*alpha + 12.0)*exp_alpha_r2);

				} else {

//					cout << format("xc[%d]: %d yc[%d]: %d zc[%d]: %d\n") % j % xc[j] % j % yc[j] % j % zc[j];
					// DX
					dxphi[0] += coeff*(pow(RAy, yc[j])*pow(RAz, zc[j])*(- 2.0*pow(RAx, xc[j] + 1)*alpha)*
													exp_alpha_r2);

					if (xc[j] != 0) {
						// DX
						dxphi[0] += coeff*(pow(RAy, yc[j])*pow(RAz, zc[j])*(pow(RAx, xc[j] - 1)*xc[j])*
														exp_alpha_r2);
						dxphi[1] += coeff*(pow(RAx, xc[j] - 2)*pow(RAy, yc[j])*pow(RAz, zc[j])*(-2*pow(RAx, 2)*alpha*(-2*pow(RAx, 2)*
														alpha + 2*xc[j] + 1) + pow(xc[j], 2) - xc[j])*exp_alpha_r2);
						dxphi[2] += coeff*(pow(RAx, xc[j] - 3)*pow(RAy, yc[j])*pow(RAz, zc[j])*(pow(RAx, 4)*pow(alpha, 2)*(-8*pow(RAx, 2)*
														alpha + 12*xc[j] + 12) - 6*pow(RAx, 2)*alpha*pow(xc[j], 2) + xc[j]*(pow(xc[j], 2) - 3*xc[j] + 2))*
														exp_alpha_r2);
						dxphi[3] += coeff*(pow(RAx, xc[j] - 4)*pow(RAy, yc[j])*pow(RAz, zc[j])*(pow(RAx, 4)*pow(alpha, 2)*(16*pow(RAx, 4)*
														pow(alpha, 2) - 32*pow(RAx, 2)*alpha*xc[j] - 48*pow(RAx, 2)*alpha + 24*pow(xc[j], 2) + 24*xc[j] + 12)+ 
														4*pow(RAx, 2)*alpha*xc[j]*(-2*pow(xc[j], 2) + 3*xc[j] - 1) + xc[j]*(pow(xc[j], 3) - 6*pow(xc[j], 2) + 
														11*xc[j] - 6))*exp_alpha_r2);
					}

					dyphi[0] += coeff*(pow(RAx, xc[j])*pow(RAz, zc[j])*(- 2.0*pow(RAy, yc[j] + 1)*alpha)*
													exp_alpha_r2);

					if (yc[j] != 0) {
						// DY
						dyphi[0] += coeff*(pow(RAx, xc[j])*pow(RAz, zc[j])*(pow(RAy, yc[j] - 1)*yc[j])*
														exp_alpha_r2);
						dyphi[1] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j] - 2)*pow(RAz, zc[j])*(-2*pow(RAy, 2)*alpha*(-2*pow(RAy, 2)*
														alpha + 2*yc[j] + 1) + pow(yc[j], 2) - yc[j])*exp_alpha_r2);
						dyphi[2] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j] - 3)*pow(RAz, zc[j])*(pow(RAy, 4)*pow(alpha, 2)*(-8*pow(RAy, 2)*
														alpha + 12*yc[j] + 12) - 6*pow(RAy, 2)*alpha*pow(yc[j], 2) + yc[j]*(pow(yc[j], 2) - 3*yc[j] + 2))*
														exp_alpha_r2);
						dyphi[3] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j] - 4)*pow(RAz, zc[j])*(pow(RAy, 4)*pow(alpha, 2)*(16*pow(RAy, 4)*
														pow(alpha, 2) - 32*pow(RAy, 2)*alpha*yc[j] - 48*pow(RAy, 2)*alpha + 24*pow(yc[j], 2) + 24*yc[j] + 12) + 
														4*pow(RAy, 2)*alpha*yc[j]*(-2*pow(yc[j], 2) + 3*yc[j] - 1) + yc[j]*(pow(yc[j], 3) - 6*pow(yc[j], 2) + 
														11*yc[j] - 6))*exp_alpha_r2);
					}

					dzphi[0] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j])*(- 2.0*pow(RAz, zc[j] + 1)*alpha)*
													exp_alpha_r2);

					if (zc[j] != 0) {
						// DZ
						dzphi[0] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j])*(pow(RAz, zc[j] - 1)*zc[j])*
														exp_alpha_r2);
						dzphi[1] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j])*pow(RAz, zc[j] - 2)*(-2*pow(RAz, 2)*alpha*(-2*pow(RAz, 2)*
														alpha + 2*zc[j] + 1) + pow(zc[j], 2) - zc[j])*exp_alpha_r2);
						dzphi[2] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j])*pow(RAz, zc[j] - 3)*(pow(RAz, 4)*pow(alpha, 2)*(-8*pow(RAz, 2)*
														alpha + 12*zc[j] + 12) - 6*pow(RAz, 2)*alpha*pow(zc[j], 2) + zc[j]*(pow(zc[j], 2) - 3*zc[j] + 2))*
														exp_alpha_r2);
						dzphi[3] += coeff*(pow(RAx, xc[j])*pow(RAy, yc[j])*pow(RAz, zc[j] - 4)*(pow(RAz, 4)*pow(alpha, 2)*(16*pow(RAz, 4)*
														pow(alpha, 2) - 32*pow(RAz, 2)*alpha*zc[j] - 48*pow(RAz, 2)*alpha + 24*pow(zc[j], 2) + 24*zc[j] + 12) + 
														4*pow(RAz, 2)*alpha*zc[j]*(-2*pow(zc[j], 2) + 3*zc[j] - 1) + zc[j]*(pow(zc[j], 3) - 6*pow(zc[j], 2) + 
														11*zc[j] - 6))*exp_alpha_r2);
					}
//					cout << format("dxphi[0]: %e dyphi[0]: %e dzphi[0]: %e\n") % dxphi[0] % dyphi[0] % dzphi[0];
				} // am1 
		} // nprim
	}	// ncontr
	return;
}


void Grid::basis_info(const GridData& gdata)
{
	cout << format("\n\nTotal number of shells: %d\n\n") % (obs_.size());

	int count = 1;
	// Iterate over shells
	for (auto& ith: obs_) {
		
		auto nprim_ith = ith.nprim();
		auto ncontr_ith = ith.ncontr();

		// Center of this shell
		auto& A = ith.O;

		cout << format("Shell number: %d\n") % count++;
		cout << format("Shell centered at: %e %e %e\n") % A[0] % A[1] % A[2];
		cout << format("Grid point: %e %e %e\n") % gdata.point[0] % gdata.point[1] % gdata.point[1];
		cout << format("AM: %d\tAM Symbol: %s\n") % ith.contr[0].l % ith.am_symbol(ith.contr[0].l);
		cout << format("Number of contractions: %d\n") % ncontr_ith;
		cout << format("Number of primitives: %d\n") % nprim_ith;

		int am0 = ith.contr[0].l;
		for (int k0 = 0; k0 <= am0; ++k0) {
			for (int n0 = 0; n0 <= k0; ++n0) {
				const int m0 = k0 - n0;
				const int l0 = am0 - k0;
		
				cout << format("l0: %d m0: %d n0: %d\n") % l0 % m0 % n0;
				for (int i = 0;i < ncontr_ith;i++) {
					for (int j = 0;j < nprim_ith;j++) {

						// access alpha's and contraction coefficients
						auto alpha1 = ith.alpha[j];
						auto coeff1 = ith.contr[0].coeff[j];

						auto RAx = (gdata.point[0] - A[0]);
						auto RAy = (gdata.point[1] - A[1]);
						auto RAz = (gdata.point[2] - A[2]);

						cout << format("Alpha: %e\t") % alpha1;
						cout << format("Coefficient: %e\t") % coeff1;

						auto alpha_r2 = alpha1 * pow(RAx + RAy + RAz, 2);

						auto gf = pow(RAx, l0) * pow(RAy, m0) * pow(RAz, n0) * exp(- alpha_r2);

						cout << format("gf: %e\n") % gf;
				
					}
				}
			}
		}
		cout << format("<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
	}
}

