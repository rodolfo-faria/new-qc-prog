#ifndef GRID_H
#define GRID_H

#include "defs.hpp"
#include "radialint.hpp"
#include "angularint.hpp"
#include "functional.hpp"
#include <boost/shared_ptr.hpp>
/*
const double BraggRadius[] = {
		0.0,
		0.35,0.35,1.45,1.05,0.85,0.70,0.65,0.60,0.50,0.50,
		1.80,1.50,1.25,1.10,1.00,1.00,1.00,1.00,2.20,1.80,
		1.60,1.40,1.35,1.40,1.40,1.40,1.35,1.35,1.35,1.35,
		1.30,1.25,1.15,1.15,1.15,1.15,2.35,2.00,1.80,1.55,
		1.45,1.45,1.35,1.30,1.35,1.40,1.60,1.55,1.55,1.45,
		1.45,1.40,1.40,1.40,2.60,2.15,1.95,1.85,1.85,1.85,
		1.85,1.85,1.85,1.80,1.75,1.75,1.75,1.75,1.75,1.75,
		1.75,1.55,1.45,1.35,1.35,1.30,1.35,1.35,1.35,1.50,
		1.90,1.80,1.60,1.90,1.90,1.90,2.60,2.15,1.95,1.80,
		1.80,1.75,1.75,1.75,1.75,1.75,1.75,1.75,1.75,1.75,
		1.75,1.75,1.75,1.55,1.55
    };
*/
const double BraggRadius[] = {
		0.00, 0.25, 0.20, 1.45, 1.05, 0.85, 0.70, 0.65, 0.60, 0.50, 0.45,
		1.80, 1.50, 1.25, 1.10, 1.00, 1.00, 1.00, 1.00, 2.20, 1.80, 1.60,
		1.40, 1.35, 1.40, 1.40, 1.40, 1.35, 1.35, 1.35, 1.35, 1.30, 1.25,
		1.15, 1.15, 1.15, 0.00, 2.35, 2.00, 1.80, 1.55, 1.45, 1.45, 1.35,
		1.30, 1.35, 1.40, 1.60, 1.55, 1.55, 1.45, 1.45, 1.40, 1.40, 0.00,
		2.60, 2.15, 1.95, 1.85, 1.85, 1.85, 1.85, 1.85, 1.85, 1.80, 1.75,
		1.75, 1.75, 1.75, 1.75, 1.75, 1.75, 1.55, 1.45, 1.35, 1.35, 1.30,
		1.35, 1.35, 1.35, 1.50, 1.90, 1.80, 1.60, 1.90, 0.00, 0.00, 0.00,
		2.15, 1.95, 1.80, 1.80, 1.75, 1.75, 1.75, 1.75, 0.00, 0.00, 0.00,
		0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00
};

//class RadialQuadrature;
//class LegendreQuadrature;
//class AngularQuadrature;
//class LebedevLaikovQuadrature;


// This structure holds grid's data
// the point used in integrations (x,y,z) and the weight
struct GridData {
	std::array<double, 3> point;
	double x;
	double y;
	double z;
	double weight;

	// rho
	double rho;
	double rho_alpha;
	double rho_beta;
	// gradient of rho
	double gradrho;
	double gradrho_alpha;
	double gradrho_beta;
	// laplacian of rho
	double laprho;
	// biharmonic of rho
	double bihrho;
	// kinetic energy density
	double tau;

	double dfdrho_alpha;
	double dfdrho_beta;
	double dfdgrad_alphaalpha;
	double dfdgrad_alphabeta;
	double dfdgrad_betabeta;
	
	
	double exd;
	double ecd;
};

// Grid Abstract Base Class 
class Grid {

	protected:

	public:

		int nrad_;
		int nang_;
		AtomVector atoms_;
		BasisSet obs_;
		bool polarized_;
		Matrix P_alpha_;
		Matrix P_beta_;

		// member initializer list
		Grid(const AtomVector& atoms, const BasisSet& obs, Matrix& P_alpha, Matrix& P_beta, const int nrad, const int nang, bool polarized) : 
						atoms_(atoms), obs_(obs), P_alpha_(P_alpha), P_beta_(P_beta),nrad_(nrad), nang_(nang), polarized_(polarized) {};

		// the computation of the grid returns a key variable to the evaluation of any other function/functional that depends
		// on the points and functions of these points such as the density, gradient of the density and etc.
		// check the structure GridData above.
		std::vector<GridData> compute();
		void basis_info(const GridData& gdata);
		void basis_values(GridData& gdata,  Shell& obs1, double& bv, std::array<double, 4>& dxphi, std::array<double, 4>& dyphi, std::array<double, 4>& dzphi);
		// compute density and its derivatives
		void compute_dnd(GridData& gdata);
		void compute_density(GridData& gdata, const Matrix& P, double& rho, std::array<double, 3>& gradrho, double& laprho, double& bihrho, double& tau);

};

// BeckeGrid is a derived class from Grid and implements
// the Grid proposed By Becke in
// 
class BeckeWeight {

	private:
		AtomVector atoms_;
		double Hyperboloid(const Atom& a, const Atom& b);
		double Step_Function(double vij);
		double F3(double uij);
		double TTP(double uij);
		double Distance(const Atom& a, const std::array<double,3>& b);
		double Distance(const Atom& a, const Atom& b);
		bool Isatomequal(const Atom& a, const Atom&b);
		void RM(const int atomic_number, double& radx);
		double Cell_Function(const Atom& ajth, const GridData& gdata);
	public:
		double Weight(const Atom& aith, const GridData& gdata);

		// member initializer list
		BeckeWeight(const AtomVector& atoms) : 
				atoms_(atoms) {}; 
};

#endif
