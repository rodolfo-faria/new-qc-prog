#include "uhf.hpp"
#include "int2e.hpp"
#include "defs.hpp"

using namespace std;

UHF::UHF(const AtomVector& atoms, const BasisSet& obs, const int nalpha, const int nbeta) : atoms_(atoms), obs_(obs), nalpha_(nalpha), nbeta_(nbeta)
{
  // count the number of electrons
	for (int i = 0; i < atoms_.size(); ++i)
		nelectron_ += atoms_[i].atomic_number;

	ndocc_ = nelectron_ / 2;
	cout << format("Maximum Number of Iterations:%d\nAccuracy:%e\n") % maxiter_ % accuracy_;
	cout << format("Number of basis functions:%d\n\n") % obs_.nbf();
}

double UHF::compute()
{
	Int1e int1e;
	Int2e int2e;

	// get the number of basis functions
	const auto nbf = obs_.nbf();
	cout << format("nalpha_: %d nbeta_: %d\n") % nalpha_ % nbeta_;

	Matrix S = int1e.compute_overlap(obs_, atoms_);
  cout << format("\n\tOverlap Integrals:\n");
  cout << S << endl;

	Matrix T = int1e.compute_kinetic(obs_, atoms_);
  cout << format("\n\tKinetic Integrals:\n");
  cout << T << endl;

	Matrix V = int1e.compute_nuclear(obs_, atoms_);
	cout << format("\n\tNuclear Integrals:\n");
  cout << V << endl;

  // Core Hamiltonian = T + V
  Matrix H = T + V;
  cout << format("\n\tCore Hamiltonian:\n");
  cout << H << endl;

  // T and V no longer needed, free up the memory
  T.resize(0,0);
  V.resize(0,0);

	// solve H C = e S C
	Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver(H, S);

	auto eps_alpha = gen_eig_solver.eigenvalues();
	auto C_alpha = gen_eig_solver.eigenvectors();

	auto eps_beta = eps_alpha;
	auto C_beta = C_alpha;

	Matrix D_alpha, D_beta, D;

	//compute density, D = C(occ) . C(occ)T
	// se the guess for D's
	auto D_mat_alpha = C_alpha.leftCols(nalpha_);
	D_alpha = D_mat_alpha * D_mat_alpha.transpose();

	auto D_mat_beta = C_beta.leftCols(nbeta_);
	D_beta = D_mat_beta * D_mat_beta.transpose();

	// Form guess D
	// eq 3.444 
	D = D_alpha + D_beta;

//	if (debug_) {
		cout << format("\n\tInitial C alpha Matrix:\n") << C_alpha << endl;
		cout << format("\n\tInitial C beta Matrix:\n") << C_beta << endl;

		cout << format("\n\tInitial D alpha Matrix:\n")  << D_alpha << endl;
		cout << format("\n\tInitial D beta Matrix:\n")  << D_beta << endl;

		cout << format("\n\tTrace of Matrix:\n");
		cout << D.trace() << endl;
//	}

	const auto conv 	 				= 1e-12;
	auto iter 				 				= 0;
	auto rmsd 				 				= 0.0;
	auto delta_energy		      = 0.0;
	auto hf_energy 			 		  = 0.0;
	auto hf_energy_alpha 		  = 0.0;
	auto hf_energy_beta	 		  = 0.0;
	auto relative_energy_diff = 0.0;

	auto rms_error = 1.0;
	auto n2 = D.cols() * D.rows();
	libint2::DIIS<Matrix> diis(2); // start DIIS on second iteration

	// prepare for incremental Fock build
	Matrix D_diff = D;
	auto F_alpha  = H;
	auto F_beta   = H;

	bool reset_incremental_fock_formation = false;
	bool incremental_Fbuild_started = false;
	double start_incremental_F_threshold = 1e-5;
	double next_reset_threshold = 0.0;
	size_t last_reset_iteration = 0;

	cout << format("\n\nStarting SCF Iterations\n\n");

  do
  {
  	++iter;

  	// save previous energy and density
  	auto hf_energy_prev	= hf_energy;
  	auto D_prev = D;

  	// Fock matrix alpha contribution
		F_alpha = H;
		F_alpha += int2e.compute_UG2(obs_, D_alpha, D_beta);

  	// Fock matrix beta contribution
		F_beta = H;
		F_beta += int2e.compute_UG2(obs_, D_beta, D_alpha);

		// solve F C = e S C
		Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver_alpha(F_alpha, S);
		auto eps_alpha = gen_eig_solver_alpha.eigenvalues();
		auto C_alpha = gen_eig_solver_alpha.eigenvectors();
		auto D_mat_alpha = C_alpha.leftCols(nalpha_);
		D_alpha = D_mat_alpha * D_mat_alpha.transpose();

		Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver_beta(F_beta, S);
		auto eps_beta = gen_eig_solver_beta.eigenvalues();
		auto C_beta = gen_eig_solver_beta.eigenvectors();
		auto D_mat_beta = C_beta.leftCols(nbeta_);
		D_beta = D_mat_beta * D_mat_beta.transpose();

		D = D_alpha + D_beta;
		// compute HF energy
		hf_energy = 0.0;
		for (auto i = 0; i < nbf; i++)
			for (auto j = 0; j < nbf; j++)
				hf_energy += (D(i,j)*H(i,j) + D_alpha(i,j)*F_alpha(i,j) + D_beta(i,j)*F_beta(i,j));
//				hf_energy += D_alpha(i,j)*(H(i,j) + F_alpha(i,j)) + D_beta(i,j)*(H(i,j) + F_beta(i,j));

		// compute energy difference and density norm
		delta_energy = hf_energy - hf_energy_prev;
//		rmsd = (D - D_prev).norm();

		if (debug_) {
			cout << "\nF alpha:\n" << F_alpha << endl;
			cout << "\nF beta:\n" << F_beta << endl;
			cout << "\nC alpha: \n" << C_alpha<< endl;
			cout << "\nC beta: \n" << C_beta << endl;
			cout << "\nD alpha: \n" << D_alpha << endl;
			cout << "\nD beta: \n" << D_beta << endl;
		}
		cout << format("iter: %d     delta energy: %e rmsd: %e energy: %8.12e\n") % iter % delta_energy % rmsd % (0.5*hf_energy);

  } while ((fabs(delta_energy) > this->get_accuracy()) && (iter < this->get_maxiter()));
//  } while (((fabs(delta_energy) > this->get_accuracy()) || (fabs(rmsd) > this->get_accuracy())) && (iter < this->get_maxiter()));

//	if (debug_) {
//	  cout << format("\t\nFinal Fock Alpha Matrix:\n") << F_alpha << endl;
//	  cout << format("\t\nFinal Fock Beta Matrix:\n") << F_beta << endl;
//	}

  auto nuclear_energy = int1e.compute_nuclear_repulsion(atoms_);

  cout << format("Trace of D:%d\n") % (D.trace());
	cout << format("Nuclear energy: %8.12e\n") % nuclear_energy;
	cout << format("Electronic energy: %8.12e\n") % (0.5*hf_energy);
	cout << format("Final Energy: %8.12e\n") % (0.5*hf_energy + nuclear_energy);

	return (0.5*hf_energy + nuclear_energy);
}

