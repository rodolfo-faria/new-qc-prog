#ifndef ATOM_H
#define ATOM_H

#include <string>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <boost/format.hpp>

struct ElementData {
    const char *symbol;
    const char *name;
    double mass; // in g/mol
    double exactmass; // in g/mol
    double ionizationenergy; // in eV
    double electronaffinity; // in eV
    double electronnegativity; // pauling scale
    double covalentradius; // in angstroms
    double vanderwaalsradius; // in angstroms
    double boilingPoint; // in kelvin
    double meltingPoint; // in kelvin
    int    atomicNumber;
};

class Element {

	private:
		int iatom_;

	public:

		Element();
		~Element();

		// set the atom by symbol
		int set_element(std::string& symbol);

		// get_element returns an atom's index in the ElementTable using atom's symbol
		unsigned int lookup_element(std::string& symbol);
		
		// get atom's name
		std::string get_name();

		// get atom's symbol
		std::string get_symbol();

		// get the atomic number of an atom by using its atomic number
		int get_Z();

		// print information of this atom
		void print();

		// print general information of the ith atom in the periodic table (just for fun)
		void print(int iatom);

};
#endif
