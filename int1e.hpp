#ifndef INT1E_H
#define INT1E_H

#include <cmath>

#include "defs.hpp"

class Int1e {

	private:

		Matrix compute_obint(const BasisSet& obs, const AtomVector& atoms, OperatorType op_type);
		
	public:

		Matrix compute_overlap(const BasisSet& obs, const AtomVector& atoms);
		Matrix compute_kinetic(const BasisSet& obs, const AtomVector& atoms);
		Matrix compute_nuclear(const BasisSet& obs, const AtomVector& atoms);
		double compute_nuclear_repulsion(const AtomVector& atoms);
};
#endif
