#ifndef UKS_H
#define UKS_H

#include "scf.hpp"
#include "defs.hpp"
#include "grid.hpp"

#include <libint2/diis.h>

class UKS: public SCF {

	private:
		int nelectron_;
		int ndocc_;
		int nalpha_;
		int nbeta_;
		AtomVector atoms_;
		BasisSet obs_;
		bool toggle_diis_;

	public:
		UKS(const AtomVector& atoms, const BasisSet& obs, const int nalpha, const int nbeta);

		// SCF cycle 
		double compute();
};
#endif 
