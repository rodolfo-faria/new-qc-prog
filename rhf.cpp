#include "rhf.hpp"
#include "int2e.hpp"
#include "defs.hpp"

using namespace std;

RHF::RHF(const AtomVector& atoms, const BasisSet& obs)
	: atoms_(atoms), obs_(obs)
{
	nelectron_ = 0;
	ndocc_ = 0;

  // count the number of electrons
	for (auto aith: atoms_)
		nelectron_ += aith.atomic_number;

	ndocc_ = nelectron_ / 2;
	cout << format("Number of electrons:%d \nNumber of doubly occupied orbitals:%d\n") % nelectron_ % ndocc_;
	cout << format("Maximum Number of Iterations:%d\nAccuracy:%e\n") % maxiter_ % accuracy_;
	cout << format("Number of basis functions:%d\n\n") % obs_.nbf();
}

double RHF::compute()
{
	Int1e int1e;
	Int2e int2e;

	// get the number of basis functions
	const auto nbf = obs_.nbf();

	Matrix S = int1e.compute_overlap(obs_, atoms_);
  cout << format("\n\tOverlap Integrals:\n");
  cout << S << endl;

	Matrix T = int1e.compute_kinetic(obs_, atoms_);
  cout << format("\n\tKinetic Integrals:\n");
  cout << T << endl;

	Matrix V = int1e.compute_nuclear(obs_, atoms_);
	cout << format("\n\tNuclear Integrals:\n");
  cout << V << endl;

  // Core Hamiltonian = T + V
  Matrix H = T + V;
  cout << format("\n\tCore Hamiltonian:\n");
  cout << H << endl;

  // T and V no longer needed, free up the memory
  T.resize(0,0);
  V.resize(0,0);

	// solve H C = e S C
	Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver(H, S);
	auto eps = gen_eig_solver.eigenvalues();
	auto C = gen_eig_solver.eigenvectors();

	//compute density, D = C(occ) . C(occ)T
	Matrix D;
	auto C_occ = C.leftCols(ndocc_);
	D = C_occ * C_occ.transpose();

	if (debug_) {
		cout << format("\n\tInitial C Matrix:\n");
		cout << C << endl;

		cout << format("\n\tInitial D Matrix:\n");
		cout << D << endl;

		cout << format("\n\tTrace of Matrix:\n");
		cout << D.trace() << endl;
	}

	const auto conv 	 				= 1e-12;
	auto iter 				 				= 0;
	auto rmsd 				 				= 0.0;
	auto delta_energy		      = 0.0;
	auto hf_energy 			 		  = 0.0;
	auto relative_energy_diff = 0.0;

	auto rms_error = 1.0;
	auto n2 = D.cols() * D.rows();
	libint2::DIIS<Matrix> diis(2); // start DIIS on second iteration

	// prepare for incremental Fock build
	Matrix D_diff = D;
	auto F = H;

	bool reset_incremental_fock_formation = false;
	bool incremental_Fbuild_started = false;
	double start_incremental_F_threshold = 1e-5;
	double next_reset_threshold = 0.0;
	size_t last_reset_iteration = 0;

	cout << format("\n\nStarting SCF Iterations\n\n");

  do
  {
  	++iter;

  	// save previous energy and density
  	auto hf_energy_prev	= hf_energy;
  	Matrix D_prev				= D;

  	if (toggle_diis_) {
  		if (not incremental_Fbuild_started && rms_error < start_incremental_F_threshold) {
				incremental_Fbuild_started = true;
				reset_incremental_fock_formation = false;
				last_reset_iteration = iter - 1;
				next_reset_threshold = rms_error / 1e1;
				if (debug_)
					cout << format("\tstarted incremental fock build\n");
			}
			if (reset_incremental_fock_formation || not incremental_Fbuild_started) {
				F = H;
				D_diff = D;
			}
			if (reset_incremental_fock_formation && incremental_Fbuild_started) {
					reset_incremental_fock_formation = false;
					last_reset_iteration = iter;
					next_reset_threshold = rms_error / 1e1;
					if (debug_)
						cout << format("\treset incremental fock build\n");
			}
  	}

  	// create a new Fock matrix
		F = H;
		F += int2e.compute_RG(obs_, D);

		// calculate the HF electronic energy
		hf_energy = D.cwiseProduct(H+F).sum();
		relative_energy_diff = std::abs((hf_energy - hf_energy_prev)/hf_energy);

		// create the eigen solver object
		Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver;

		if (toggle_diis_) {

		  //compute SCF error
			Matrix FD_comm = F*D*S - S*D*F;
			rms_error = FD_comm.norm()/n2;
			// store only 8 iterations
			if (rms_error < next_reset_threshold || iter - last_reset_iteration >= 8)
				reset_incremental_fock_formation = true;

			// DIIS extrapolate F
			Matrix F_diis = F; // extrapolated F cannot be used in incremental Fock build; only used to produce the density
												 // make a copy of the unextrapolated matrix
			diis.extrapolate(F_diis,FD_comm);

			// solve F C = e S C
			gen_eig_solver.compute(F_diis, S);

		}	else {

			// solve F C = e S C
			gen_eig_solver.compute(F, S);
		}

		auto eps = gen_eig_solver.eigenvalues();
		auto C = gen_eig_solver.eigenvectors();

		// compute density, D = C(occ) . C(occ)T
		auto C_occ = C.leftCols(ndocc_);
		D = C_occ * C_occ.transpose();
		D_diff = D - D_prev;

		// compute density norm
		if (not toggle_diis_)
			rms_error = (D - D_prev).norm();

		if (debug_) {
			cout << "\nF: \n" << F << endl;
			cout << "\nC: \n" << C << endl;
			cout << "\nD: \n" << D << endl;
		}
		if (iter < 2)
			cout << "  Iteration    |       E      |    Delta(E)   |   RMS error   |" << endl;

		cout << format("%-16.8f %-16.8f %-16.8f %-16.8f\n") % iter % hf_energy % relative_energy_diff % rms_error;

  } while (((relative_energy_diff > this->get_accuracy()) || (rms_error > this->get_accuracy())) && (iter < this->get_maxiter()));

  // check for convergence
  if (iter == this->get_maxiter()) {
  	converged_ = false;
  	cerr << "\t\n*** SCF not converged ***" << endl;
  } else {
  	converged_ = true;
		D_ = D;
		S_ = S;
  }


	if (debug_)
	  cout << format("\t\nFinal Fock Matrix:\n") << F << endl;

	auto nuclear_energy = int1e.compute_nuclear_repulsion(atoms_);

  cout << format("Trace of D:%d\n") % (2*D.trace());
	cout << format("Final Energy: %8.12e\n") % (hf_energy + nuclear_energy);

	return (hf_energy + nuclear_energy);
}



