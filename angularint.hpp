#ifndef ANGULAR_QUADRATURE_H
#define ANGULAR_QUADRATURE_H

#include <vector>
#include <cmath>


#define EPS 3.0e-11

class AngularQuadrature {

	protected:
		int nang_;
	public:
		AngularQuadrature(const int nang) : nang_(nang) {};
		virtual void compute(std::vector<double>& x, std::vector<double>& y, std::vector<double>& z, std::vector<double>& w) = 0;
};

class LebedevLaikovQuadrature: public AngularQuadrature {

	private:

		void GEN_OH(int code, int &num, std::vector<double>& x, std::vector<double>& y, std::vector<double>& z, std::vector<double>& w, double a, double b, double v);

		void LD0194(std::vector<double>& x, std::vector<double>& y, std::vector<double>& z, std::vector<double>& w);

	public:
		LebedevLaikovQuadrature(const int nang) : AngularQuadrature(nang) {};
		void compute(std::vector<double>& x, std::vector<double>& y, std::vector<double>& z, std::vector<double>& w);

};

class GaussLegendreQuadrature {

	public:

		void compute(double x1, double x2, std::vector<double>& x, std::vector<double>& w, int n);
};

#endif
