#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

#include "defs.hpp"
#include "grid.hpp"
#include <cmath>
#include <string>


const std::array<std::string, 3> xfunctionals = {"LSDAX", "B88X", "B3X"};
const std::array<std::string, 3> cfunctionals = {"LSDAC", "VWNC", "PW91C"};


// abstract class functional
class Functional {

	public:
		virtual double compute_energy(struct GridData& gdata) = 0;
};


class LSDAX:public Functional {
	
	private:
		bool polarized_;
		bool compute_potential_;
	public:
		double compute_energy(struct GridData& gdata);
		LSDAX(bool polarized, bool compute_potential);
};

class SlaterX:public Functional {
	
	private:
		bool polarized_;
		bool compute_potential_;
		double alpha_;
	public:
		double compute_energy(struct GridData& gdata);
		SlaterX(bool polarized, bool compute_potential);
};

class B88X:public Functional {
	private:
		bool polarized_;
		bool compute_potential_;
	public:
		double compute_energy(struct GridData& gdata);
		B88X(bool polarized, bool compute_potential);
		
};

class VWN5C:public Functional {
	private:
		bool polarized_;
		bool compute_potential_;

		double Ap_;
		double Af_;
		double A_alpha_;

		double x0p_mc_;
		double bp_mc_;
		double cp_mc_;
		double x0f_mc_;
		double bf_mc_;
		double cf_mc_;
		double x0_alpha_mc_;
		double b_alpha_mc_;
		double c_alpha_mc_;
		
		double x0p_rpa_;
		double bp_rpa_;
		double cp_rpa_;
		double x0f_rpa_;
		double bf_rpa_;
		double cf_rpa_;
		double x0_alpha_rpa_;
		double b_alpha_rpa_;
		double c_alpha_rpa_;

	public:
		double compute_energy(struct GridData& gdata);
		double F(double x, double A, double x0, double b, double c);
		double dFdr_s(double x, double A, double x0, double b, double c);
		VWN5C(bool polarized, bool compute_potential);
		
};

typedef boost::shared_ptr<Functional> Functional_Ptr;

class DFA {

		private:
		bool polarized_;
		bool compute_potential_;
		std::string xfunctional_;
		std::string cfunctional_;
		unsigned short xfunctional_ingredients_;
		unsigned short cfunctional_ingredients_;

	public:	

		std::vector<Functional_Ptr> XFunctionalVector;
		std::vector<Functional_Ptr> CFunctionalVector;

		bool polarized() { return polarized_;};
		std::string get_xfunctional() { return xfunctional_; };
		std::string get_cfunctional() { return cfunctional_; };
		void set_xfunctional(std::string xfunctional) { xfunctional_ = xfunctional; };
		void set_cfunctional(std::string cfunctional) { cfunctional_ = cfunctional; };
		void compute(struct GridData& gdata);

		DFA(std::string xfunctional, std::string cfunctional, bool polarized, bool compute_potential);

};

#endif
