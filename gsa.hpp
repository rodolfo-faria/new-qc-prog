#ifndef GSA_HPP_
#define GSA_HPP_

#include "defs.hpp"
#include "int1e.hpp"
#include "int2e.hpp"
#include "energy.hpp"

class GSA: public Energy {

	private:

		double qAccentance_; 		// Acceptance index
		double qTemperature_;		// Temperature index
		double qVisiting_;			// Visiting index
		double dimension_;			// Dimension
		double maxiter_;				// maximum number of iterations
		double Tinitial_;				// Initial temperature
		double accuracy_;				// Energy accuracy
		int    ncycles_;				// number of GSA cycles

		bool   converged_;
		bool	 guess_hcore;
		int    nelectron_;
		int 	 ndocc_;

		AtomVector atoms_;
		BasisSet obs_;

		void   init();
		Matrix hcore_guess(MatrixRef H);
		void   g_function(double temperature, double Tup, MatrixConstRef C_0, MatrixRef C_t,
										  double coef, double exponent1, double exponent2, double qv1);
		double rhf_energy(MatrixConstRef C, MatrixConstRef H, Int2e& int2e);

	public:

		GSA(const AtomVector& atoms, const BasisSet& obs);

		double energy();
		double gsa_engine();

};



#endif /* GSA_HPP_ */




