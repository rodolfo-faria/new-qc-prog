#ifndef INPUT_H
#define INPUT_H
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdio>
#include <cctype>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/tokenizer.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string/find.hpp>

class Input {

	private:

		// vector of tokens 	
		std::vector<std::string> tokens_;
		std::string bsname_;
		std::string method_;
		std::string xyzfile_;
		int natoms_;
		int multiplicity_;
		int charge_;

		// As the name says, tokenize each line of the input file
		int tokenize_input(std::istream& inputfile);
		// Read input file of a single atom
		int read_tokens_input();
		// Change all characters to uppercase
		std::string upperCase(std::string input);

	public:

		int read(std::istream& inputfile);
		
		// get the name of the method
		std::string get_method() {	return method_;	};

		// get basis set name
		std::string get_bsname() {	return bsname_;	};

		std::string get_xyzfile() {	return xyzfile_;	};

		unsigned int get_natoms() {	return natoms_;	};

		unsigned int get_multiplicity() {	return multiplicity_;	};

		int get_charge() { return charge_; };

};


#endif
