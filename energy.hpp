#ifndef ENERGY_HPP_
#define ENERGY_HPP_

#include "defs.hpp"

class Energy {

	public:

		virtual double energy() = 0;

};



#endif /* ENERGY_HPP_ */
