#ifndef MPA_HPP_
#define MPA_HPP_

#include "pop_analysis.hpp"
#include "defs.hpp"

class MPA: public PopulationAnalysis {

	public:
		void compute(MatrixConstRef D, MatrixConstRef S);
};

#endif /* MPA_HPP_ */
