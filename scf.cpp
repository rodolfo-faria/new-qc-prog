#include "scf.hpp"
#include "defs.hpp"

using namespace std;

SCF::SCF()
{
	maxiter_ = 100;
	accuracy_ = 1.e-12;
	debug_ = false;
	converged_ = false;
	toggle_diis_ = true;
}

SCF::SCF(const SCF& n)
{
	maxiter_ = n.maxiter_;
	accuracy_ = n.accuracy_;
}

void SCF::set_maxiter(const int maxiter)
{
	maxiter_ = maxiter;
	return;
}

void SCF::set_accuracy(const double accuracy)
{
	accuracy_ = accuracy;
	return;
}

int SCF::get_maxiter()
{
	return maxiter_;
}

double SCF::get_accuracy()
{
	return accuracy_;
}

void SCF::print()
{
	cout << format("Maximum of SCF iterations: %d\n") % this->get_maxiter();
	cout << format("SCF accuracy: %e\n") % this->get_accuracy();
	return;
}

bool SCF::convergence()
{
	return converged_;
}

Matrix SCF::get_overlap()
{
	return S_;
}

Matrix SCF::get_density()
{
	return D_;
}

